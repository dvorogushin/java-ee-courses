package repository;

import javax.imageio.stream.FileImageOutputStream;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Date: 20.11.2021
 * Project: Lesson_19
 * <p>
 * Производит чтение и запись в/из файла filename.
 * При чтении возвращает коллекцию экземпляров User.
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class UsersRepositoryImpl implements UsersRepository {

    private String filename;

    public UsersRepositoryImpl(String filename) {
        this.filename = filename;
    }

    /**
     * Читает файл построчно.
     * Из каждой строки извлекаются значения полей класса User.
     * Возвращает коллекцию классов User, отфильтрованных по возрасту age.
     *
     * @param age возраст, по которому фильтруется поле User.age.
     * @return возвращает отфильтрованную коллекцию List<User>
     */
    @Override
    public List<User> findByAge(int age) {
        List<User> users = new ArrayList<>();

        // автоматический вызов методов close()
//        можно записать как минимум двумя способами:
//        try (Reader reader = new FileReader(filename); BufferedReader bufferedReader = new BufferedReader(reader)) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] parts = line.split("\\|");
                if (Integer.parseInt(parts[1]) == age) {
                    User user = new User(parts[0], Integer.parseInt(parts[1]), Boolean.parseBoolean(parts[2]));
                    users.add(user);
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
        return users;
    }

    /**
     * Читает файл filename построчно.
     * Из каждой строки извлекаются значения полей класса User.
     *
     * @return Возвращает коллекцию классов User, отфильтрованных по полю User.isWorker.
     */
    @Override
    public List<User> findByIsWorkerIsTrue() {
        Reader reader = null;
        BufferedReader bufferedReader = null;
        List<User> users = new ArrayList<>();
        try {
            reader = new FileReader(filename);
            bufferedReader = new BufferedReader(reader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] parts = line.split("\\|");
                if (Boolean.parseBoolean(parts[2])) {
                    User user = new User(parts[0], Integer.parseInt(parts[1]), Boolean.parseBoolean(parts[2]));
                    users.add(user);
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ignore) {
                }
            }
        }
        return users;
    }

    /**
     * Читает файл filename построчно.
     * Из каждой строки извлекаются значения полей класса User.
     *
     * @return Возвращает коллекцию классов User.
     */
    @Override
    public List<User> findAll() {
        Reader reader = null;
        BufferedReader bufferedReader = null;
        List<User> users = new ArrayList<>();
        try {
            reader = new FileReader(filename);
            bufferedReader = new BufferedReader(reader);
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                User user = new User(parts[0], Integer.parseInt(parts[1]), Boolean.parseBoolean(parts[2]));
                users.add(user);
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ignore) {
                }
            }
        }
        return users;
    }

    /**
     * Записывает в файл filename строчку, соответствующую принятому экземпляру User.
     * Строчка формируется полями класса User c разделителем "\".
     *
     * @param user записываемый в файл экземпляр User
     * @return result - true если не были вызваны исключения при записи в файл
     */
    @Override
    public boolean save(User user) {

        // экспериментальный блок для личной проверки PrintWriter
        try (PrintWriter writer1 = new PrintWriter(System.out)) {
            writer1.println("Метод System.out.println, вызванный через PrintWriter в методе Save() в середине программы." +
                    "\nПочему-то выполняется в конце всего Main()");
        }

        boolean result = false;
        Writer writer = null;
        BufferedWriter bufferedWriter = null;

        try {
            writer = new FileWriter(filename, true);
            bufferedWriter = new BufferedWriter(writer);
            bufferedWriter.write(user.getName() + "|" + user.getAge() + "|" + user.isWorker());
            bufferedWriter.newLine();
            bufferedWriter.flush();
            result = true;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException ignore) {
                }
            }

        }
        return result;
    }
}
