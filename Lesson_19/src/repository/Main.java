package repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Date: 20.11.2021
 * Project: Lesson_19
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class Main {
    static void printList(List<User> col) {
        for (User user : col) {
            System.out.println("name: " + user.getName() + "\t age: " + user.getAge() + "\t isWorker: " + user.isWorker());
        }

    }

    public static void main(String[] args) {

        // Создаем экземпляр класса, читающего переданный текстовый файл
        UsersRepository usersRepository = new UsersRepositoryImpl("src/repository/users.txt");
        // Вызываем метод, который читает файл построчно и возвращает созданный экземпляр коллекции классов
        List<User> users = usersRepository.findAll();
        System.out.println("Result of method \"findAll\":");
        printList(users);

        // Создаем экземпляр класса User
        User newUser = new User("Дмитрий", 26, true);
        // Вызываем метод, записывающий нового User в текстовый файл в виде последне строки
        usersRepository.save(newUser);

        // Вызываем метод, читающий файл построчно и возвращающий экземпляр коллекции классов User,
        // отфильтрованных по возрасту
        List<User> users1 = usersRepository.findByAge(27);
        System.out.println("\nResult of method \"findByAge(27)\":");
        printList(users1);

        // Вызываем метод, читающий файл построчно и возвращающий экземпляр коллекции классов User,
        // отфильтрованных по полю isWorker
        List<User> users2 = usersRepository.findByIsWorkerIsTrue();
        System.out.println("\nResult of method \"findByIsWorkerIsTrue\":");
        printList(users2);


    }
}
