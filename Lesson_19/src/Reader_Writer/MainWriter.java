package Reader_Writer;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class MainWriter {
    public static void main(String[] args) {
        String FILE_PATH = "src/Reader_Writer/";
        try {
            Writer fileWriter = new FileWriter(FILE_PATH + "outputStream.txt");
            fileWriter.write("SomeString");
            fileWriter.close();
        } catch (IOException e) {
            System.err.println("IOException " + e.getMessage());
        }
    }
}
