package Reader_Writer;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class MainBufferedWriter {
    public static void main(String[] args) {
        String FILE_PATH = "src/Reader_Writer/";
        try {
            FileWriter writer = new FileWriter(FILE_PATH + "outputStream.txt");
            BufferedWriter bufferedWriter = new BufferedWriter(writer);
            bufferedWriter.write("Some text.");
            bufferedWriter.close();
        } catch (IOException e) {
            System.err.println("IOException " + e.getMessage());
        }
    }
}
