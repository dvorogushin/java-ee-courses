package Reader_Writer;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class MainInputStream {
    public static void main(String[] args) {
        String FILE_PATH = "src/Reader_Writer/";
        try {
            InputStream inputStream = new FileInputStream(FILE_PATH + "inputStream.txt");
//            byte[] bytes = inputStream.readAllBytes();
//            System.out.println(Arrays.toString(bytes));

            int currentByte = inputStream.read();
            while(currentByte != -1) {
                System.out.print((char)currentByte);
                currentByte = inputStream.read();

            }
        } catch (FileNotFoundException e) {

            System.err.println("FileNotFoundException " + e.getMessage());
        } catch (IOException e) {
            System.err.println("IOException " + e.getMessage());

        }
    }
}
