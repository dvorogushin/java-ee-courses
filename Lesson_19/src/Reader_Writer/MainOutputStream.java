package Reader_Writer;

import java.io.*;

public class MainOutputStream {

    public static void main(String[] args) {
        String FILE_PATH = "src/Reader_Writer/";
        try {
            OutputStream outputStream = new FileOutputStream(FILE_PATH + "outputStream.txt");
            byte[] bytes = "Hello".getBytes();
            outputStream.write(bytes);
        } catch (FileNotFoundException e) {
            System.err.println("FileNotFoundException " + e.getMessage());
        } catch (IOException e) {
            System.err.println("IOException " + e.getMessage());
        }

    }
}
