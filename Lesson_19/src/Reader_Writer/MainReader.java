package Reader_Writer;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class MainReader {
    public static void main(String[] args) {
        String FILE_PATH = "src/Reader_Writer/";
        try {
            Reader reader = new FileReader(FILE_PATH + "inputStream.txt");
            int currentChar = reader.read();
            while (currentChar != -1) {
                System.out.print((char)currentChar);
                currentChar = reader.read();
            }
        } catch (FileNotFoundException e) {
            System.err.println("File not found " + e.getMessage());
        } catch (IOException e) {
            System.err.println("Read exception " + e.getMessage());
        }
    }
}
