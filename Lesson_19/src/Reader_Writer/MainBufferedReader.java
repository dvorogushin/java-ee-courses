package Reader_Writer;

import java.io.*;

public class MainBufferedReader {
    public static void main(String[] args) {
        String FILE_PATH = "src/Reader_Writer/";
        try {
            Reader reader = new FileReader(FILE_PATH + "inputStream.txt");
            BufferedReader bufferedReader = new BufferedReader(reader);
            String str; // = bufferedReader.readLine();

//            while (str != null) {
//                System.out.println(str);
//                str = bufferedReader.readLine();
//
//            }
            // тоже самое, но в одну строчку (metanit)
            while ((str = bufferedReader.readLine()) != null) System.out.println(str);


        } catch (FileNotFoundException e) {
            System.err.println("File not found. " + e.getMessage());
        } catch (IOException e) {
            System.err.println("IOException " + e.getMessage());
        }
    }
}
