## В пакете [repository](src/repository) представлен класс [UsersRepositoryImpl](src/repository/UsersRepositoryImpl.java), реализующий интерфейс [UsersRepository](src/repository/UsersRepository.java). 
## В классе реализованы методы чтения и записи текстовых данных в/из файла.
## В результате парсинга файла возвращается коллекция классов List\<User>.