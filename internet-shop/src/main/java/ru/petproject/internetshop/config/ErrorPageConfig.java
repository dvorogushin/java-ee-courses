package ru.petproject.internetshop.config;

import org.springframework.boot.web.server.ErrorPage;
import org.springframework.boot.web.server.ErrorPageRegistrar;
import org.springframework.boot.web.server.ErrorPageRegistry;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;

/**
 * Date: 30.12.2021
 * Project: internet-shop
 * Обработчик ошибок
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Configuration
public class ErrorPageConfig implements ErrorPageRegistrar {

 @Override
 public void registerErrorPages(ErrorPageRegistry registry) {

  // если статус NOT_FOUND, то в контроллер на "/error/404"
  ErrorPage page404 = new ErrorPage(HttpStatus.NOT_FOUND, "/error/404");
  ErrorPage page500 = new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, "/error/500");
  // регистрация страницы
  registry.addErrorPages(page404);
  registry.addErrorPages(page500);
 }
}
