package ru.petproject.internetshop.forms;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * Date: 26.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Setter
@Getter
@Builder
public class ProductForm {

    private Long id;

    @NotNull
    @Length(min = 2, max = 100)
    private String name;

    private String description;

    @NotNull
    private Double price;

    @NotNull
    private Integer quantity;

}
