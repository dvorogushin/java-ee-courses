package ru.petproject.internetshop.forms;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * Date: 26.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Getter
@Setter
@Builder
public class SignUpForm {

    @NotNull
    @Length(min = 2, max = 20)
    private String firstName;

    @Length(min = 2, max = 20)
    private String lastName;

    @NotNull
    @Length(min = 5, max = 30)
    private String email;

    @NotNull
    private String password;
}
