package ru.petproject.internetshop.models;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Date: 15.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity(name = "account")
//@ToString(exclude = {"createdCompanies", "createdProducts", "createdOrders", "paidOrders"})
public class    User {

    public enum Role {
        USER, ADMIN, MANAGER;
    }

    public enum State {
        NOT_CONFIRMED, CONFIRMED, BANNED, DELETED;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition = "TIMESTAMP NOT NULL")
    private LocalDateTime createDateTime;

    @Column(columnDefinition = "VARCHAR (30) NOT NULL UNIQUE")
    private String email;

    @Column(columnDefinition = "VARCHAR (100) NOT NULL")
    private String hashPassword;

    @Column(columnDefinition = "VARCHAR (20)")
    private String firstName;

    @Column(columnDefinition = "VARCHAR (20)")
    private String lastName;

    @ManyToOne
    @JoinColumn(name = "company_id")
    private Company company;

    @Enumerated(value = EnumType.STRING)
    @Column(columnDefinition = "VARCHAR (20) NOT NULL")
    private State state;

    @Enumerated(value = EnumType.STRING)
    @Column(columnDefinition = "VARCHAR (20) NOT NULL")
    private Role role;

    @OneToMany(mappedBy = "createUser")
    @ToString.Exclude
    private List<Company> createdCompanies;

    @OneToMany(mappedBy = "createUser")
    @ToString.Exclude
    private List<Product> createdProducts;

    @OneToMany(mappedBy = "createUser")
    @ToString.Exclude
    private List<Order> createdOrders;

    @OneToMany(mappedBy = "payUser")
    @ToString.Exclude
    private List<Order> paidOrders;

    @OneToMany(mappedBy = "uploadUser")
    @ToString.Exclude
    private List<FileInfo> uploadedFiles;

    public boolean isEmployeeOfOrderCompany(Order order) {
        if ((this.getCompany() != null)
                && (order.getCompany() != null)) {
            return this.getCompany().getId().equals(order.getCompany().getId());
        }
        return false;
    }

    public boolean isManager() {
        return this.getRole().equals(Role.MANAGER);
    }
}
