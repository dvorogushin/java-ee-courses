package ru.petproject.internetshop.models;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Date: 15.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Product {

    public enum State {
        AVAILABLE, DELETED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition = "TIMESTAMP NOT NULL")
    private LocalDateTime createDateTime;

    @ManyToOne
    @JoinColumn(name = "create_user_id", columnDefinition = "BIGINT NOT NULL")
    private User createUser;

    @Column(columnDefinition = "VARCHAR (100) NOT NULL")
    private String name;

    private String description;

    @Column(columnDefinition = "DOUBLE PRECISION NOT NULL CHECK (price >= 0)")
    private Double price;

    @Column(columnDefinition = "INTEGER NOT NULL CHECK (quantity >= 0)")
    private Integer quantity;

    @Enumerated(value = EnumType.STRING)
    @Column(columnDefinition = "VARCHAR (20) NOT NULL")
    private State state;

    @OneToMany(mappedBy = "product")
    private List<Order> orders;

    @OneToMany(mappedBy = "product")
    private List<FileInfo> files;

}
