package ru.petproject.internetshop.models;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Date: 16.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity(name = "order_list")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition = "TIMESTAMP NOT NULL")
    private LocalDateTime createDateTime;

    @ManyToOne
    @JoinColumn(name = "create_user_id", columnDefinition = "BIGINT NOT NULL")
    private User createUser;

    @Column(name = "payment_date_time")
    private LocalDateTime paymentDateTime;

    @ManyToOne
    @JoinColumn(name = "pay_user_id")
    private User payUser;


    @Column(columnDefinition = "INTEGER NOT NULL DEFAULT 0 CHECK (quantity >= 0)")
    private Integer quantity;

    @Column(columnDefinition = "INTEGER NOT NULL DEFAULT 0 CHECK (amount >= 0)")
    private Integer amount;

    @ManyToOne
    @JoinColumn(name = "company_id", columnDefinition = "BIGINT")
    private Company company;

    @ManyToOne
    @JoinColumn(name = "product_id", columnDefinition = "BIGINT NOT NULL")
    private Product product;

    public boolean hasCompany() {
        return this.getCompany() != null;
    }

}
