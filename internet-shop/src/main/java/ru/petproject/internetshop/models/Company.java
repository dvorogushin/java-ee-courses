package ru.petproject.internetshop.models;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Date: 16.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@ToString(exclude = {"orders", "employees"})
public class Company {

    public enum State {
        NOT_CONFIRMED, CONFIRMED, BANNED, DELETED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition = "TIMESTAMP NOT NULL")
    private LocalDateTime createDateTime;

    @ManyToOne
    @JoinColumn(name = "create_user_id", columnDefinition = "BIGINT NOT NULL")
    private User createUser;

    @Column(columnDefinition = "VARCHAR(100) NOT NULL")
    private String name;

    private String address;

    @Column(columnDefinition = "VARCHAR (50)")
    private String inn;

    @Enumerated(value = EnumType.STRING)
    @Column(columnDefinition = "VARCHAR (20) NOT NULL")
    private State state;

    @OneToMany(mappedBy = "company")
    private List<User> employees;

    @OneToMany(mappedBy = "company")
    private List<Order> orders;
}
