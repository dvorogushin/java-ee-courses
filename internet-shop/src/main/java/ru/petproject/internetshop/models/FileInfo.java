package ru.petproject.internetshop.models;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Date: 27.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity(name = "file")
public class FileInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition = "TIMESTAMP NOT NULL")
    private LocalDateTime uploadDateTime;

    @Column(columnDefinition = "VARCHAR (50) NOT NULL UNIQUE")
    private String storageName;

    @Column(columnDefinition = "VARCHAR (50)")
    private String originalName;

    // формат файла - img/jpeg
    @Column(columnDefinition = "VARCHAR (10) NOT NULL")
    private String mimeType;

    private Long size;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    @ManyToOne
    @JoinColumn(name = "upload_user_id")
    private User uploadUser;
}
