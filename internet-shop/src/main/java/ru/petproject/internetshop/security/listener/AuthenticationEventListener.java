package ru.petproject.internetshop.security.listener;

import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AbstractAuthenticationEvent;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

/**
 * Date: 06.01.2022
 * Project: internet-shop
 *
 * Логгер попыток и результата аутентификации
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Component
public class AuthenticationEventListener implements ApplicationListener<AbstractAuthenticationEvent> {
    @Override
    public void onApplicationEvent(AbstractAuthenticationEvent authenticationEvent) {
        if (authenticationEvent instanceof InteractiveAuthenticationSuccessEvent) {
            return;
        }
        Authentication authentication = authenticationEvent.getAuthentication();
        String auditMessage = "Login attempt with username: " +
                authentication.getName() + ", " +
                authentication.getAuthorities() + ", " +
                authentication.getCredentials() + ", " +
                authentication.getPrincipal() + ", " +
                "\t\t Success: " + authentication.isAuthenticated() + "\n";
        System.out.println(auditMessage);
    }
}
