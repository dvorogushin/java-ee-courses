package ru.petproject.internetshop.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Date: 30.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class ProductNotFoundException extends RuntimeException{

}
