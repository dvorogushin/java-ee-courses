package ru.petproject.internetshop.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.petproject.internetshop.models.Product;
import ru.petproject.internetshop.services.ProductService;

import java.util.List;

/**
 * Date: 17.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@RequiredArgsConstructor
@Controller
public class ProductController {

    private final ProductService productService;

    @GetMapping("/")
    public String getProducts() {
        return "redirect:/catalog";
    }

    @GetMapping("/catalog")
    public String getProducts(Model model) {
        List<Product> products = productService.getAllProducts();
        model.addAttribute("products", products);
        return "products";
    }

    @GetMapping("products/{product-id}")
    public String getProduct(Model model, @PathVariable("product-id") Long productId) {
        Product product = productService.getProduct(productId);
        model.addAttribute("product", product);
        return "product";
    }
}
