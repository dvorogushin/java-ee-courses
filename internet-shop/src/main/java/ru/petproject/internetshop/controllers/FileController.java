package ru.petproject.internetshop.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.petproject.internetshop.services.FileInfoService;

import javax.servlet.http.HttpServletResponse;

/**
 * Date: 30.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Controller
@RequiredArgsConstructor
public class FileController {

    private final FileInfoService fileInfoService;

    @GetMapping("/files/{file:.+}")
    public void getFile(@PathVariable("file") String fileName,
                        HttpServletResponse response) {
        fileInfoService.addFileToResponse(fileName, response);
    }


}
