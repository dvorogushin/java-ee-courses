package ru.petproject.internetshop.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.petproject.internetshop.forms.SignUpForm;
import ru.petproject.internetshop.services.SignUpService;
import javax.validation.Valid;

/**
 * Date: 26.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@RequiredArgsConstructor
@Controller
@RequestMapping("/signup") // все запросы к /signup
public class SignUpController {

    private final SignUpService signUpService;

    @GetMapping
    public String getSignUpPage(Authentication auth) {
        if (auth != null) {
            return "/";
        }
        return "signup";
    }

    @PostMapping
    public String signUpCustomer(@Valid SignUpForm form,
                                 BindingResult result,
                                 RedirectAttributes forRedirectModel) {
        if (result.hasErrors()) {
            forRedirectModel.addFlashAttribute("errors", "Some errors occurred. Pls check the data you trying to send.");
            return "redirect:/signup";
        }
        signUpService.signUpUser(form);
        return "redirect:/signin";
    }


}
