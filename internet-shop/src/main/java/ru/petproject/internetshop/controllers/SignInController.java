package ru.petproject.internetshop.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * Date: 26.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Controller
@RequestMapping("/signin")
public class SignInController {

    @GetMapping
    public String getSignInPage(Model model,
                                Authentication auth,
                                HttpServletRequest request) {

        // получение параметра без значения "...?error"
        if (request.getParameterMap().containsKey("error")) {
            model.addAttribute("error", true);
        }

        if (auth != null) {
            return "/";
        }
        return "/signin";
    }
}
