package ru.petproject.internetshop.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.petproject.internetshop.models.User;
import ru.petproject.internetshop.services.UserService;

/**
 * Date: 18.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Controller
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    // кабинет пользователя
    @GetMapping("/user")
    public String getUserPage(Model model,
                              @AuthenticationPrincipal(expression = "id") Long userId) {
        User user = userService.getUser(userId);
        model.addAttribute("user", user);
        return "user";
    }

    // добавление товара в корзину = размещение заказа
    @PostMapping("/user")
    public String addProduct(Model model,
                             @RequestParam("productId") Long productId,
                             @AuthenticationPrincipal(expression = "id") Long userId) {
        userService.createOrder(userId, productId);
        User user = userService.getUser(userId);
        model.addAttribute("user", user);
        return "user";
    }

    // отмена заказа
    @PostMapping("/user/cancel")
    public String cancelOrder(Model model,
                              @RequestParam("orderId") Long orderId,
                              @AuthenticationPrincipal(expression = "id") Long userId) {
        userService.cancelOrder(userId, orderId);
        return "redirect:/user";
    }

    // оплата заказа
    @PostMapping("/user/pay")
    public String payOrder(Model model,
                           @RequestParam("orderId") Long orderId,
                           @AuthenticationPrincipal(expression = "id") Long userId) {
        userService.payOrder(userId, orderId);
        return "redirect:/user";
    }
}
