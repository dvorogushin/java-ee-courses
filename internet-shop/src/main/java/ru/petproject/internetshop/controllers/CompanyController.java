package ru.petproject.internetshop.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.petproject.internetshop.models.Company;
import ru.petproject.internetshop.services.CompanyService;
import ru.petproject.internetshop.services.UserService;

/**
 * Date: 23.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Controller
@RequiredArgsConstructor
public class CompanyController {

    private final CompanyService companyService;
    private final UserService userService;

    @GetMapping("/company")
    public String getCompanyPage(Model model,
                                 @AuthenticationPrincipal(expression = "id") Long userId) {
        Company company = companyService.getCompany(userId);
        model.addAttribute("company", company);
        return "company";
    }

    // оплата заказа
    @PostMapping("/company/pay")
    public String payOrder(@RequestParam("orderId") Long orderId,
                           @AuthenticationPrincipal(expression = "id") Long userId) {
        userService.payOrder(userId, orderId);
        return "redirect:/company";
    }

    // отмена заказа
    @PostMapping("/company/cancel")
    public String cancelOrder(@RequestParam("orderId") Long orderId,
        @AuthenticationPrincipal(expression = "id") Long userId) {
        userService.cancelOrder(userId, orderId);
        return "redirect:/company";
    }
}
