package ru.petproject.internetshop.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.petproject.internetshop.forms.ProductForm;
import ru.petproject.internetshop.models.Order;
import ru.petproject.internetshop.models.Product;
import ru.petproject.internetshop.models.User;
import ru.petproject.internetshop.services.FileInfoService;
import ru.petproject.internetshop.services.ProductService;
import ru.petproject.internetshop.services.UserService;

import javax.validation.Valid;
import java.util.List;

/**
 * Date: 25.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Controller
@RequiredArgsConstructor
public class ManagerController {

    private final UserService userService;
    private final ProductService productService;
    private final FileInfoService fileInfoService;

//    // кабинет пользователя
//    @GetMapping("/user")
//    public String getUserPage(Model model) {
//        User user = userService.getUser(1L);
//        model.addAttribute("user", user);
//        return "user";
//    }

    // кабинет менеджера, только POST
    @GetMapping("/manager")
    public String getPage(Model model,
                          @AuthenticationPrincipal(expression = "id") Long userId) {
        User user = userService.getUser(userId);
        List<Order> orders = userService.getAllOrders(userId);
        model.addAttribute("user", user);
        model.addAttribute("orders", orders);
        return "manager/manager";
    }

    // отмена заказа
    @PostMapping("/manager/cancel")
    public String cancelOrder(@RequestParam("orderId") Long orderId,
                              @AuthenticationPrincipal(expression = "id") Long userId) {
        userService.cancelOrder(userId, orderId);
        return "redirect:/manager";
    }

    // страница продуктов для менеджера
    @GetMapping("/manager/products")
    public String getProducts(Model model,
                              @AuthenticationPrincipal(expression = "id") Long userId) {
        User user = userService.getUser(userId);
        List<Product> products = productService.getAllProducts();
        model.addAttribute("products", products);
        model.addAttribute("user", user);
        return "manager/products_add";
    }

    // добавление товара и сохранение фото
    @PostMapping("/manager/products/add")
    public String addProduct(@Valid ProductForm form,
                             BindingResult result,
                             RedirectAttributes forRedirectModel,
                             @AuthenticationPrincipal(expression = "id") Long userId,
                             @RequestParam("files") MultipartFile[] files) {
        if (result.hasErrors()) {
            forRedirectModel.addFlashAttribute("errors", "Some errors occurred. Pls check the data you trying to send.");
            return "redirect:/manager/products";
        }
        // сохранение товара
        Product product = productService.addProduct(userId, form);
//        Product product = productsService.getProduct(1L);
        // сохранение фото
        for (MultipartFile file : files) {
            if (!file.isEmpty()) {
                fileInfoService.save(file, userId, product);
            }
        }
        return "redirect:/manager/products";
    }

    // изменение товара
    @PostMapping("/manager/products/change")
    public String changeProduct(@Valid ProductForm form,
                                BindingResult result,
                                RedirectAttributes forRedirectModel,
                                @AuthenticationPrincipal(expression = "id") Long userId,
                                 @RequestParam("files") MultipartFile[] files) {
        if (result.hasErrors()) {
            forRedirectModel.addFlashAttribute("errors", "Some errors occurred. Pls check the data you trying to send.");
            return "redirect:/manager/products";
        }
        productService.changeProduct(userId, form);
        Product product = productService.getProduct(form.getId());
        // сохранение фото
        for (MultipartFile file : files) {
            if (!file.isEmpty()) {
                fileInfoService.save(file, userId, product);
            }
        }
        return "redirect:/manager/products";
    }

    // страница для работы с товаром для менеджера
    @GetMapping("/manager/products/{product-id}")
    public String getProduct(Model model,
                             @PathVariable("product-id") Long productId,
                             @AuthenticationPrincipal(expression = "id") Long userId) {
        User user = userService.getUser(userId);
        Product product = productService.getProduct(productId);
        model.addAttribute("user", user);
        model.addAttribute("product", product);
        return "manager/product_edit";
    }

    // удаление фотографии
    @PostMapping("/manager/products/{product-id}")
    public String delFile(Model model,
                          @PathVariable("product-id") Long productId,
                          @RequestParam("fileStorageName") String fileName) {
        fileInfoService.deleteFile(fileName);
        return "redirect:/manager/products/" + productId;
    }


}
