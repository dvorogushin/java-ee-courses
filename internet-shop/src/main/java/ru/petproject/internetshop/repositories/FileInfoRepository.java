package ru.petproject.internetshop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.petproject.internetshop.models.FileInfo;

import java.util.Optional;

/**
 * Date: 28.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public interface FileInfoRepository extends JpaRepository<FileInfo, Long> {
    Optional<FileInfo> findByStorageName(String storageName);
}
