package ru.petproject.internetshop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.petproject.internetshop.models.Order;
import ru.petproject.internetshop.models.User;

import java.util.List;

/**
 * Date: 18.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public interface OrderRepository extends JpaRepository<Order, Long> {
    List<Order> findAllByCreateUser_Id(Long userId);
}
