package ru.petproject.internetshop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.petproject.internetshop.models.Company;

/**
 * Date: 23.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public interface CompanyRepository extends JpaRepository<Company, Long> {
}
