package ru.petproject.internetshop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.petproject.internetshop.models.User;

import java.util.Optional;

/**
 * Date: 18.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByEmail(String email);
}
