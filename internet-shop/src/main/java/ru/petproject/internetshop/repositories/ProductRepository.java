package ru.petproject.internetshop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.petproject.internetshop.models.Product;

import java.util.List;

/**
 * Date: 17.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */

public interface ProductRepository extends JpaRepository<Product, Long> {
    List<Product> findAllByState(Product.State available);
}
