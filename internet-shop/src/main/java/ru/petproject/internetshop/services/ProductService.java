package ru.petproject.internetshop.services;

import ru.petproject.internetshop.forms.ProductForm;
import ru.petproject.internetshop.models.Product;

import java.util.List;

/**
 * Date: 17.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */

public interface ProductService {

    List<Product> getAllProducts();

    Product getProduct(Long productId);

    Product addProduct(Long userId, ProductForm productForm);

    void changeProduct(Long userId, ProductForm productForm);

}
