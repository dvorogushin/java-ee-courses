package ru.petproject.internetshop.services;

import ru.petproject.internetshop.models.Company;

/**
 * Date: 23.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public interface CompanyService {
    Company getCompany(Long companyId);
}
