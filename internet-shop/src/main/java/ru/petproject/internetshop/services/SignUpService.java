package ru.petproject.internetshop.services;

import ru.petproject.internetshop.forms.SignUpForm;

/**
 * Date: 26.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */

public interface SignUpService {
    void signUpUser (SignUpForm form);
}
