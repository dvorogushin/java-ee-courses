package ru.petproject.internetshop.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.petproject.internetshop.exceptions.ProductNotFoundException;
import ru.petproject.internetshop.forms.ProductForm;
import ru.petproject.internetshop.models.Product;
import ru.petproject.internetshop.repositories.ProductRepository;
import ru.petproject.internetshop.repositories.UserRepository;
import ru.petproject.internetshop.services.ProductService;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Date: 17.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Service
@RequiredArgsConstructor
public class ProductsServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final UserRepository userRepository;

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAllByState(Product.State.AVAILABLE);
    }

    @Override
    public Product getProduct(Long productId) {
        return productRepository.findById(productId).orElseThrow(ProductNotFoundException::new);
    }

    @Override
    public Product addProduct(Long userId, ProductForm productForm) {
        Product product = Product.builder()
                .name(productForm.getName())
                .description(productForm.getDescription())
                .price(productForm.getPrice())
                .quantity(productForm.getQuantity())
                .createDateTime(LocalDateTime.now())
                .createUser(userRepository.getById(userId))
                .state(Product.State.AVAILABLE)
                .build();
        productRepository.save(product);
        return product;
    }

    /**
     * Изменение товара (пока только количества)
     * @param userId
     * @param productForm
     */
    @Override
    public void changeProduct(Long userId, ProductForm productForm) {
        Product product = getProduct(productForm.getId());
        product.setQuantity(productForm.getQuantity());
        productRepository.save(product);
    }
}
