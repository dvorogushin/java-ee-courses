package ru.petproject.internetshop.services;

import ru.petproject.internetshop.models.Order;
import ru.petproject.internetshop.models.User;

import java.util.List;

/**
 * Date: 18.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */


public interface UserService {
    User getUser(Long userId);

    void createOrder(Long userId, Long productId);

    void cancelOrder(Long userId, Long orderId);

    void payOrder(Long userId, Long orderId);

    List<Order> getAllOrders(Long userId);
}
