package ru.petproject.internetshop.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.petproject.internetshop.exceptions.OrderNotFoundException;
import ru.petproject.internetshop.exceptions.ProductNotFoundException;
import ru.petproject.internetshop.models.Order;
import ru.petproject.internetshop.models.Product;
import ru.petproject.internetshop.models.User;
import ru.petproject.internetshop.repositories.OrderRepository;
import ru.petproject.internetshop.repositories.ProductRepository;
import ru.petproject.internetshop.repositories.UserRepository;
import ru.petproject.internetshop.services.UserService;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Date: 18.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final OrderRepository orderRepository;
    private final ProductRepository productsRepository;

    @Override
    public User getUser(Long id) {
        User user = userRepository.getById(id);
        return user;
    }

    @Override
    public void createOrder(Long userId, Long productId) {
        User user = userRepository.getById(userId);
        Product product = productsRepository
                .findById(productId)
                .orElseThrow(ProductNotFoundException::new);
        Order order = Order.builder()
                .createDateTime(LocalDateTime.now())
                .product(product)
                .createUser(user)
                .company(user.getCompany())
                .amount(product.getPrice().intValue())
                .quantity(1)
                .build();
        orderRepository.save(order);
        product.setQuantity(product.getQuantity() - 1);
        productsRepository.save(product);
    }

    @Override
    public void cancelOrder(Long userId, Long orderId) {
        // удалять заказ может любой аутентифицированный пользователь
        Order order = orderRepository.findById(orderId).orElseThrow(OrderNotFoundException::new);
        Product product = productsRepository.getById(order.getProduct().getId());
        product.setQuantity(product.getQuantity() + 1);
        productsRepository.save(product);
        orderRepository.delete(order);
    }

    /**
     * Заказ может оплатить
     * 1: user, у которого user.company совпадает с order.company
     * 2: если order.company is null, то user, который совпадает с order.createUser
     * @param userId
     * @param orderId
     */
    @Override
    public void payOrder(Long userId, Long orderId) {
        User user = userRepository.getById(userId);
        Order order = orderRepository.findById(orderId).orElseThrow(OrderNotFoundException::new);
        // если у заказа есть компания (владелец), то user должен быть из этой компании
        if (order.hasCompany()) {
            if (user.isEmployeeOfOrderCompany(order)) {
                // оплата
                order.setPayUser(user);
                order.setPaymentDateTime(LocalDateTime.now());
                orderRepository.save(order);
            }
            // TODO Вы не можете оплатить заказ так как не работаете в компании владельце заказа
            return;
        }
        // если у заказа нет компании, то user должен совпадать с order.createUser
        if (user.equals(order.getCreateUser())) {
            // оплата
            order.setPayUser(user);
            order.setPaymentDateTime(LocalDateTime.now());
            order.setCompany(user.getCompany());
            orderRepository.save(order);
        }
    }

    /**
     * Для менеджера список включает все заказы
     * @param userId
     * @return
     */
    @Override
    public List<Order> getAllOrders(Long userId) {
        User user = userRepository.getById(userId);
        if (user.isManager()) {
            return orderRepository.findAll();
        }
        return orderRepository.findAllByCreateUser_Id(userId);
    }
}
