package ru.petproject.internetshop.services;

import org.springframework.web.multipart.MultipartFile;
import ru.petproject.internetshop.models.Product;

import javax.servlet.http.HttpServletResponse;

/**
 * Date: 28.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public interface FileInfoService {
    void save(MultipartFile file, Long userId, Product product);

    void addFileToResponse(String fileName, HttpServletResponse response);

    void deleteFile(String fileName);
}

