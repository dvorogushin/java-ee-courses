package ru.petproject.internetshop.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.petproject.internetshop.forms.SignUpForm;
import ru.petproject.internetshop.models.User;
import ru.petproject.internetshop.repositories.UserRepository;
import ru.petproject.internetshop.services.SignUpService;

import java.time.LocalDateTime;

/**
 * Date: 26.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Service
@RequiredArgsConstructor
public class SignUpServiceImpl implements SignUpService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;

    @Override
    public void signUpUser(SignUpForm form) {
        User user = User.builder()
                .firstName(form.getFirstName())
                .lastName((form.getLastName()))
                .email(form.getEmail())
                .hashPassword(passwordEncoder.encode(form.getPassword()))
                .createDateTime(LocalDateTime.now())
                .role(User.Role.USER)
                .state(User.State.NOT_CONFIRMED)
                .build();
        userRepository.save(user);
    }
}
