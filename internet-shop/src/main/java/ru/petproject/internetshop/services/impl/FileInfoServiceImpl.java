package ru.petproject.internetshop.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.petproject.internetshop.exceptions.FileInfoNotFoundException;
import ru.petproject.internetshop.models.FileInfo;
import ru.petproject.internetshop.models.Product;
import ru.petproject.internetshop.repositories.FileInfoRepository;
import ru.petproject.internetshop.repositories.UserRepository;
import ru.petproject.internetshop.services.FileInfoService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Date: 28.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Service
@RequiredArgsConstructor
public class FileInfoServiceImpl implements FileInfoService {

    private final FileInfoRepository fileInfoRepository;
    private final UserRepository userRepository;

    @Value("${files.storage.path}")
    private String storageFolder;

    /**
     * Сохранение файла.
     * @param file
     * @param userId
     * @param product
     */
    @Override
    public void save(MultipartFile file, Long userId, Product product) {
        try {
            String originalFileName = file.getOriginalFilename();
            String extension = originalFileName.substring(originalFileName.indexOf("."));
            FileInfo fileInfo = FileInfo.builder()
                    .originalName(file.getOriginalFilename())
                    .mimeType(file.getContentType())
                    .product(product)
                    .uploadDateTime(LocalDateTime.now())
                    .uploadUser(userRepository.getById(userId))
                    .storageName(UUID.randomUUID() + extension)
                    .size(file.getSize())
                    .build();

            Files.copy(file.getInputStream(),
                    Paths.get(storageFolder).resolve(fileInfo.getStorageName()));
            fileInfoRepository.save(fileInfo);
        } catch (IOException e) {
            throw new IllegalArgumentException("Could not store the file. Error: " + e.getMessage());
        }
    }

    /**
     *  Получение файла по названию в storageName и отправка в response.
     * @param fileName
     * @param response
     */
    @Override
    public void addFileToResponse(String fileName, HttpServletResponse response) {
        FileInfo fileInfo = fileInfoRepository.findByStorageName(fileName).orElseThrow(FileInfoNotFoundException::new   );
        // сообщения браузеру: тип, размер, оригинальное название
        response.setContentType(fileInfo.getMimeType());
        response.setContentLength(fileInfo.getSize().intValue());
        response.setHeader("Content-Disposition", "filename=\"" + fileInfo.getOriginalName() + "\"");
        try {
            // копирование файла в response
            Files.copy(Paths.get(storageFolder).resolve(fileInfo.getStorageName()), response.getOutputStream());
        } catch (IOException e) {
            throw new IllegalArgumentException("Can't find the file " + fileName + " Error: " + e.getMessage());
        }
    }

    /**
     * Удаление файла.
     * @param fileName
     */
    @Override
    public void deleteFile(String fileName) {
        FileInfo fileInfo = fileInfoRepository.findByStorageName(fileName).orElseThrow(FileInfoNotFoundException::new);
        fileInfoRepository.delete(fileInfo);
    }
}
