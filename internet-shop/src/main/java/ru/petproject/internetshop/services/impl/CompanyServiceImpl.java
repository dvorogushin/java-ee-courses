package ru.petproject.internetshop.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.petproject.internetshop.models.Company;
import ru.petproject.internetshop.repositories.CompanyRepository;
import ru.petproject.internetshop.services.CompanyService;

/**
 * Date: 23.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Service
@RequiredArgsConstructor
public class CompanyServiceImpl implements CompanyService {

    private final CompanyRepository companyRepository;

    @Override
    public Company getCompany(Long companyId) {
        return companyRepository.getById(companyId);
    }
}
