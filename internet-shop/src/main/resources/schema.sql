-- создание таблицы учета сессий для remember me
create table if not exists persistent_logins
(
    username  varchar(64) not null,
    series    varchar(64) not null,
    token     varchar(64) not null,
    last_used timestamp   not null
);

-- создание таблицы товаров
create table if not exists product
(
    id             bigserial primary key,
    create_date_time timestamp,
    name           varchar(100),
    description    varchar,
    price          DOUBLE PRECISION check ( price >= 0 ),
    quantity       integer check (quantity >= 0 ),
    state          varchar(20) not null,
    create_user_id bigint      not null,
    foreign key (create_user_id) references account (id)
);

-- заполнение таблицы товаров
-- insert into product (create_date_time, name, description, price, quantity, state, create_user_id)
-- VALUES ('2021-12-17 09:55:04', 'Компьютер', 'Компьютер большой Intel', 50000, 10, 'AVAILABLE', 1);
-- insert into product (create_date_time, name, description, price, quantity, state, create_user_id)
-- VALUES ('2021-12-17 09:55:04', 'Компьютер', 'Компьютер маленький Intel', 40000, 10, 'AVAILABLE', 1);
-- insert into product (create_date_time, name, description, price, quantity, state, create_user_id)
-- VALUES ('2021-12-17 09:55:04', 'Компьютер', 'Компьютер большой Mac', 60000, 10, 'AVAILABLE', 1);
-- insert into product (create_date_time, name, description, price, quantity, state, create_user_id)
-- VALUES ('2021-12-17 09:55:04', 'Компьютер', 'Компьютер малый Mac', 45000, 10, 'AVAILABLE', 1);
-- insert into product (create_date_time, name, description, price, quantity, state, create_user_id)
-- VALUES ('2021-12-17 09:55:04', 'Ноутбук', 'Ноутбук большой Mac', 50000, 10, 'AVAILABLE', 1);
-- insert into product (create_date_time, name, description, price, quantity, state, create_user_id)
-- VALUES ('2021-12-17 09:55:04', 'Ноутбук', 'Ноутбук малый Mac', 65000, 10, 'AVAILABLE', 1);
-- insert into product (create_date_time, name, description, price, quantity, state, create_user_id)
-- VALUES ('2021-12-17 09:55:04', 'Ноутбук', 'Ноутбук большой Intel', 75000, 10, 'AVAILABLE', 1);
-- insert into product (create_date_time, name, description, price, quantity, state, create_user_id)
-- VALUES ('2021-12-17 09:55:04', 'Ноутбук', 'Ноутбук малый Intel', 85000, 10, 'AVAILABLE', 1);

-- создание таблицы account
create table if not exists account
(
    id               bigserial primary key,
    create_date_time timestamp   not null,
    email            varchar(30) unique,
    hash_password    varchar(100),
    first_name       varchar(20),
    lats_name        varchar(100),
    role             varchar(20) not null,
    state            varchar(20) not null,
    company_id       bigint,
    foreign key (company_id) references company (id)
);

-- заполнение таблицы account
-- insert into account (create_date_time, email, hash_password, first_name, last_name, role, state)
-- VALUES ('2021-12-17 09:55:04', 'dvorogushin@mail.ru', '1', '1', '1', 'USER', 'NOT_CONFIRMED');
-- insert into account (create_date_time, email, hash_password, first_name, last_name, role, state)
-- VALUES ('2021-12-17 09:55:04', 'dvorogushin2@mail.ru', '2', '2', '2', 'USER', 'NOT_CONFIRMED');

-- создание таблицы company
create table if not exists company
(
    id               bigserial primary key,
    create_date_time timestamp    not null,
    address          varchar(255),
    inn              varchar(50),
    name             varchar(100) not null,
    state            varchar(20)  not null,
    create_user_id   bigint,
    foreign key (create_user_id) references account (id)
);

-- заполнение таблицы company
-- insert into company (address, create_date_time, inn, name, state, create_user_id)
-- VALUES ('г.Москва', '2021-12-12', '123', 'ООО Рога и копыта', 'NOT_CONFIRMED', 1);
-- insert into company (address, create_date_time, inn, name, state, create_user_id)
-- VALUES ('г.Санкт-Петербург', '2021-12-14', '321', 'ООО Копыта и Рога', 'NOT_CONFIRMED', 1);

-- создание таблицы заказов
create table if not exists order_list
(
    id                bigserial primary key,
    create_date_time  timestamp not null,
    payment_date_time timestamp,
    quantity          integer   not null default 0 check ( quantity >= 0 ),
    amount            integer   not null default 0 check ( amount >= 0 ),
    product_id        bigint    not null,
    foreign key (product_id) references product (id),
    company_id        bigint,
    foreign key (company_id) references company (id),
    create_user_id    bigint    not null,
    foreign key (create_user_id) references account (id),
    pay_user_id       bigint    not null,
    foreign key (pay_user_id) references account (id)
);

-- заполнение таблицы заказов
-- insert into order_list (create_date_time, quantity, amount, product_id, create_user_id, company_id)
-- VALUES ('2021-12-17 09:55:04', 1, 1000, 3, 1, 1);


