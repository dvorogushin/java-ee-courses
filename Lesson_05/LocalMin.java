class LocalMin {

	public static void main(String[] args) {

		int maxRND = 48;        //верхняя граница диапазона случайных чисел, нижняя - 0
		int qtyOfLocalMin = 0;  //Количество локальных минимумов

		int currentNum = getRandomNumber(0,maxRND); //текущее случайное число
		int previousNum = 0;						//предыдущее число
		int prePreviousNum = 0;						//пред-предыдущее число

		//пока случайно не выпадет 0 
		while (currentNum > 0){

			showBar(previousNum); // отображение случайного числа и шкалы

			//главное условие - два числа слева и справа в ряду больше среднего
			if (currentNum > previousNum && previousNum < prePreviousNum){

				qtyOfLocalMin++;
				// отображение локального минимума
				showLocalMinBar(previousNum, maxRND, qtyOfLocalMin);
			}
	
			System.out.println();
			
			//очередная итерация
			prePreviousNum = previousNum;
			previousNum = currentNum;
			currentNum = getRandomNumber(0,maxRND);
		}

		//отображение последнего значения
		showBar(previousNum);
		System.out.println("\nResult: " + qtyOfLocalMin);
	}
	
	// функция выдачи случайного числа в диапазоне
	public static int getRandomNumber(int min, int max) {
	    return (int) ((Math.random() * (max - min)) + min);
	}

	//функция отображения случ.числа и шкалы
	public static void showBar(int rndNum){
		String bar = "RND: ";
		
		if (rndNum < 10){
			bar = bar + " ";
		}
		bar = bar + rndNum + " ";

		while(rndNum > 0){
			bar = bar + "D";
			rndNum--;
		}
		System.out.print(bar);
	}

	//функция отображения отметки локального минимума
	public static void showLocalMinBar(int localMin, int maxRND, int qtyOfLocalMin){
		String bar = "";
		while (localMin < maxRND){
			bar = bar + " ";
			localMin++;
		}
		System.out.print(bar + "<<< local_min #: " + qtyOfLocalMin);
	}
}