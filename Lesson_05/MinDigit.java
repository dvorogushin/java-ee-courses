import java.util.Scanner;

class MinDigit {

	public static void main(String[] args) {

		// объявляю объект для считывания чисел с консоли
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("\n * * * Поиск минимальной цифры числа * * *\n");
		System.out.print("Введите число (выход: -1): ");
		// считываю первое число
		long currentNum = scanner.nextLong();
		short globMinDigit = 10;
		short minDigit;
		short tmp;
		
		while (currentNum != (-1)){
			minDigit = 10;
			tmp = 0;
			while (currentNum != 0){
				tmp = (short) (currentNum % 10);
				if (tmp < minDigit){
					minDigit = tmp;

				}
				currentNum = currentNum/10;
			}
			// отлавливаем ввод цифры "0"
			if (minDigit == 10){minDigit = 0;}
			
			// запонимаем мин.цифру по всему ряду
			if (minDigit < globMinDigit){
				globMinDigit = minDigit;
			}

			System.out.println("                Мин.цифра: " + minDigit);
			System.out.print("\nВведите число (выход: -1): ");
			currentNum = scanner.nextLong();

		}

		System.out.println("\nМин.цифра из всех чисел: " + globMinDigit + "\n");
	}
}