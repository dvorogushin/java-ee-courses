package ru.homework;

/**
 * Date: 06.12.2021
 * Project: Lesson_26
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class NumbersUtil {

    // метод вычисляющий наибольший общий делитель
    int gcd(int n1, int n2) {
        if (n1 < 0 || n2 < 0) {
            throw new IllegalArgumentException("Numbers should be positive.");
        }

        if (n1 == 0) {return n2;}

        if (n2 == 0) {return n1;}

        int n;
        for (n = 0; ((n1 | n2) & 1) == 0; n++) {
            n1 >>= 1;
            n2 >>= 1;
        }

        while ((n1 & 1) == 0) {
            n1 >>= 1;
        }

        do {
            while ((n2 & 1) == 0) {
                n2 >>= 1;
            }

            if (n1 > n2) {
                int temp = n1;
                n1 = n2;
                n2 = temp;
            }
            n2 = (n2 - n1);
        } while (n2 != 0);
        return n1 << n;
    }
}
