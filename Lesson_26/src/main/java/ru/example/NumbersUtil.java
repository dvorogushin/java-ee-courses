package ru.example;

/**
 * Date: 05.12.2021
 * Project: Lesson_26
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class NumbersUtil {
    public boolean isPrime(int number) {

        if (number == 0 || number == 1) {
            throw new IllegalArgumentException("0 or 1");
        }

        if (number == 2 || number == 3) {
            return true;
        }

        for (int i = 2; i * i <= number; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    public int sum(int num1, int num2) {
        return num1 + num2;
    }
}

