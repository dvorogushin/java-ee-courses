package ru.example;

/**
 * Date: 05.12.2021
 * Project: Lesson_26
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class Vector2D {
    private double x;
    private double y;

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double length() {
        return Math.sqrt(x * x + y * y);
    }
}
