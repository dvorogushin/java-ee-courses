package ru.example;

import junit.framework.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Date: 05.12.2021
 * Project: Lesson_26
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class Vector2DTest {
    private final double EPS = 1e-9;
    private static Vector2D vector2D;

    @BeforeClass
    public static void createVector() {
        vector2D = new Vector2D();
    }

    @Test
    public void newVectorShouldHaveZeroLength() {
        // assertion - утверждение
        // delta при вещественных числах
        Assert.assertEquals(0, vector2D.length(), EPS);

    }

    @Test
    public void newVectorShouldHaveZeroX() {
//        Vector2D v1 = new Vector2D();
        assertEquals(0, vector2D.getX(), EPS);
    }

}
