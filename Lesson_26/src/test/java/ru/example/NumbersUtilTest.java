package ru.example;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName(value = "NumbersUtilTest is working when")
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
public class NumbersUtilTest {

    private NumbersUtil numbersUtil = new NumbersUtil();

    @Nested
    @DisplayName(value = "isPrime() is worked")
    public class ForIsPrime {

        @ParameterizedTest(name = "return <true> on {0}")
        @ValueSource(ints = {2, 3, 71, 113})
        public void on_prime_numbers_return_true(int number) {
            assertTrue(numbersUtil.isPrime(number));
        }

        @ParameterizedTest(name = "return <false> on {0}")
        @ValueSource(ints = {22, 33, 4, 8, 100})
        public void on_not_prime_numbers_return_false(int number) {
            assertFalse(numbersUtil.isPrime(number));
        }

        @ParameterizedTest(name = "return <false> on {0}")
        @ValueSource(ints = {121, 169})
        public void on_sqr_numbers_return_false(int number) {
            assertFalse(numbersUtil.isPrime(number));

        }

        @ParameterizedTest(name = "throws Exception on {0}")
        @ValueSource(ints = {0, 1})
        public void return_exception_when_ONE_or_ZERO(int number) {
            assertThrows(IllegalArgumentException.class, () -> numbersUtil.isPrime(number));
        }
    }

    @Nested
    @DisplayName(value = "sum() is worked")
    public class ForSum {

        @ParameterizedTest(name = "return {2} on {0} + {1}")
        @CsvSource(value = {"1, 2, 3", "5, 10, 15", "4, -4, 0"})
        public void return_sum(int a, int b, int result) {
            assertEquals(result, numbersUtil.sum(a, b));

        }
    }
}