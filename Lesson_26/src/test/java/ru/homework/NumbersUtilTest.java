package ru.homework;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayName(value = "NumberUtils is worked when")
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
public class NumbersUtilTest {

    NumbersUtil numbersUtil = new NumbersUtil();

    // внутренний класс, тестирующий метод gcp()
    @Nested
    @DisplayName(value = "ForGcd() is worked")
    public class ForGCD {

        // проверка срабатывания исключения при отрицательном числе
        @ParameterizedTest(name = "throw Exception for {0} and {1}")
        @CsvSource(value = {"-1, 50", "1, -50", "-1, -4"})
        public void on_negative_numbers_should_be_throw(int num1, int num2) {
            assertThrows(IllegalArgumentException.class, () -> numbersUtil.gcd(num1, num2));
        }

        // проверка некоторых вариантов
        @ParameterizedTest(name = "return {2} on {0} and {1}")
        @CsvSource(value = {"0, 4, 4", "5, 0, 5", "12, 8, 4", "24, 12, 12"})
        public void on_positive_numbers_return_gcd(int num1, int num2, int result) {
            assertEquals(result, numbersUtil.gcd(num1, num2));
        }
    }
}