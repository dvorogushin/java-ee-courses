
public class Min_Max {

    /**
     * Программа формирует массив частот выпадения случайных чисел в диапазоне от -100 до 100.
     * Максимальная частота повторений ограничена программно (до 1 000 000 раз).
     * Максимально возможная частота - 2 147 483 647 раз (4 байта).
     * 
     */

    public static void main(String[] args) {

        System.out.println("Формируется массив из случайных чисел в дапазоне от -100 до 100.");
        System.out.println("Максимальная частота повторений любого числа ограничена (до 1 000 000 раз).");
        System.out.println("Максимальная возможная частота повторений 2 147 483 647 раз (4 байта).");
        
            //массив, содержащий частоту повторений каждого числа
        int frecuency[] = new int[201];
        
        int minFreq = 1000000;
        int minDigit = 201;
        
        int maxFreq = 0; 
        int maxDigit = 201;
        
            // случайное число в заданном диапазоне
        int digit = 0;
        
            // формирование массива частот выпадения каждого числа 
            // до первого достижения максимального значения повторений любым числом 
        while(frecuency[digit] < minFreq) {
          
            digit = (int) (Math.random () * 201);
        
            frecuency[digit]++;
        
        }

        int num = 0;
        
            // цикл по массиву с частотами каждого числа
        for (int i = 0; i < frecuency.length; i++) {

            System.out.print((i - 100) + ": "+ frecuency[i] + "  ");
            
                //Вывод на экран по 5 в столбцов
            num++;
            if (num > 5){
                System.out.println();
                num = 0;
            }
            
                //поиск минимальной частоты
            if (frecuency[i] < minFreq) {
                minFreq = frecuency[i];
                minDigit = i - 100;
            }
                // поиск максимальной частоты
            if (frecuency[i] > maxFreq) {
                maxFreq = frecuency[i];
                maxDigit = i;
            }

        }

        System.out.println();
        System.out.println("Минимальное количество раз (" + minFreq + ") выпало число: " + minDigit);
        System.out.println("Максимальное количество раз (" + maxFreq + ") выпало число: " + maxDigit);
        
    }
}
