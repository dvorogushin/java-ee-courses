package ru.homework25;

import java.util.List;

/**
 * Date: 05.12.2021
 * Project: Lesson_25
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */

public interface ProductsRepository {
    List<Product> findAll();

    List<Product> findAllByPrice(double price);

    List<Product> findAllByOrdersCount(int ordersCount);
}
