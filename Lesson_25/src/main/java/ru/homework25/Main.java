package ru.homework25;

import org.springframework.jdbc.datasource.DriverManagerDataSource;
import javax.sql.DataSource;

/**
 * Date: 05.12.2021
 * Project: Lesson_25
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */

public class Main {
    public static void main(String[] args) {

        DataSource dataSource = new DriverManagerDataSource(
                "jdbc:postgresql://localhost:5432/lesson_24",
                "dmitry",
                "qwerty007");
        ProductsRepository productsRepository = new ProductsRepositoryJdbcTemplateImpl(dataSource);

        System.out.println(productsRepository.findAll());
        System.out.println(productsRepository.findAllByPrice(300));
        System.out.println(productsRepository.findAllByOrdersCount(2));
    }
}
