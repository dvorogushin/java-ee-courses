package ru.homework25;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Date: 05.12.2021
 * Project: Lesson_25
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Product {
    private Integer id;
    private String description;
    private Double price;
    private Integer quantity;
}
