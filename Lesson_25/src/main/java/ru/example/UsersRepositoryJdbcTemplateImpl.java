package ru.example;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;

public class UsersRepositoryJdbcTemplateImpl implements UsersRepository {

    //запросы в СУБД
    // внести запись
    //language=SQL
    private static final String SQL_INSERT = "insert into account (first_name, last_name, age) values(?, ?, ?)";
    // вывести всех
    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from account order by id";
//    select product_id,  count(*) from orderOf group by product_id having count(*) =2;
//
    // экземпляр класса JdbcTemplate (запросы к БД) храним внутри
    private JdbcTemplate jdbcTemplate;

    // конструктор
    public UsersRepositoryJdbcTemplateImpl(DataSource dataSource) {

        // драйвер для работы с БД PostgreSQL, запросы к БД
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    // внесение записи в БД
    @Override
    public boolean save(User user) {
        jdbcTemplate.update(SQL_INSERT,user.getFirstName(), user.getLastName(), user.getAge());
        return true;
    }


    // RowMapper<T> - правило, по которому из каждой строки достаются данные по наименованию столбца
    // Возвращет параметр Т, сформированный по правилу
    public static final RowMapper<User> userRowMapper = (row, rowNumber) -> {
        int id = row.getInt("id");
        String firstname = row.getString("first_name");
        String lastname = row.getString("last_name");
        int age = row.getInt("age");
        return new User(id, firstname, lastname, age);
    };

    // получение всех записей из таблицы с помощью RowMapper'a
    @Override
    public List<User> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, userRowMapper);
    }


}
