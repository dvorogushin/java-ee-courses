package ru.example;

import java.util.List;

/**
 * Date: 20.11.2021
 * Project: Lesson_19
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public interface UsersRepository {
    List<User> findAll();

    boolean save(User user);


}
