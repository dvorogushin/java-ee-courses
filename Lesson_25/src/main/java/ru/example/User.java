package ru.example;

import lombok.*;


/**
 * Date: 20.11.2021
 * Project: Lesson_19
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */


//@Setter
//@Getter
//@ToString
//@EqualsAndHashCode
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User {
    private int id;
    private String firstName;
    private String lastName;
    private int age;
}
