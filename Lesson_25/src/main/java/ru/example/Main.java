package ru.example;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;


public class Main {

    public static void main(String[] args) {

        // подключение к существующей базе данных
        DataSource dataSource = new DriverManagerDataSource(
                "jdbc:postgresql://localhost:5432/bobdb",
                "postgres",
                "qwerty007");

        UsersRepository usersRepository = new UsersRepositoryJdbcTemplateImpl(dataSource);

        // создаем запись в таблице
        User user = new User(0, "Ivan", "Иванов", 26);
//        usersRepository.save(user);

        // для того, чтобы не использовать id=0 при создании объекта можно использовать Builder
        User user1 = User.builder()
                .firstName("Masha")
                .lastName("Murina")
                .age(12).build();
        usersRepository.save(user1);

        // распечатываем всю таблицу
        System.out.println(usersRepository.findAll());


    }

}
