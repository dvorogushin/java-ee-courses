## Distributor ID:	Ubuntu
Description:	Ubuntu 20.04.3 LTS
Release:	20.04
Codename:	focal

* dm@DmHost1:~$ java -version 
```
openjdk version "17" 2021-09-14
OpenJDK Runtime Environment (build 17+35-Ubuntu-120.04)
OpenJDK 64-Bit Server VM (build 17+35-Ubuntu-120.04, mixed mode, sharing)
```
* dm@DmHost1:~$ javac -version
```
javac 17
```
* dm@DmHost1:~$ echo $JAVA_HOME
```
/usr/bin/
```
* dm@DmHost1:~$ echo $JRE_HOME
```
/usr/bin/
```

## Microsoft Windows [Version 10.0.17763.253]
(c) 2018 Microsoft Corporation. All rights reserved.

* C:\Users\Äìèòðèé>java -version
```
openjdk version "17.0.1" 2021-10-19
OpenJDK Runtime Environment (build 17.0.1+12-39)
OpenJDK 64-Bit Server VM (build 17.0.1+12-39, mixed mode, sharing)
```
* C:\Users\Äìèòðèé>javac -version
```
javac 17.0.1
```
* C:\Users\Äìèòðèé>echo %JAVA_HOME%
```
C:\Program Files\Java\jdk-17.0.1\
```
* C:\Users\Äìèòðèé>echo %JRE_HOME%
```
C:\Program Files\Java\jdk-17.0.1\
```
