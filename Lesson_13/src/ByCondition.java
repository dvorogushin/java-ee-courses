/**
 * Date: 07.11.2021
 * Project: Lesson_13
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public interface ByCondition {

    /*
    Метод без реализации. Гарантирует наличие у наследников наличие метода с заданой сигнатурой
     */
    boolean isOk(int number);
}
