import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        // Формирование массива произвольных чисел
        int[] arr = new int[20];
        for (int i = 1; i < arr.length - 1; i++) {
            arr[i] = (int) (Math.random() * 10000);
        }
        arr[10] = 0; // для проверки исключения 0

        System.out.println("\nSource array:");
        System.out.println(Arrays.toString(arr));

//        // переопределенный метод isOk
//        // условие возврата true - четное число (кроме 0)
//        // определение четности по младшему биту (кроме 0)
//        int[] a = Sequence.filter(arr, number -> (number != 0) && ((number & 1) != 1));
//
//        // переопределенный метод isOk
//        // условие возврата true - четное число (кроме 0)
//        // реализовано через метод isEven() текущего класса
//        int[] b = Sequence.filter(arr, number -> (number != 0) && isEven(number));
//
//        System.out.println("\nOnly evens array:");
//        System.out.println(Arrays.toString(a));
//
//        // переопределение метода isOk
//        // условие возврата true - четная сумма всех цифр числа
//        int[] c = Sequence.filter(arr, new ByCondition() {
//            @Override
//            public boolean isOk(int number) {
//
//                if (number == 0) return false;  // отлавливаем 0
//
//                boolean result = true;
//                // цикл по цифрам числа
//                while (number > 0) {
//                    // определение четности суммы цифр числа через XOR младших битов каждой цифры
//                    result ^= (((number % 10) & 1) == 1);
//                    number /= 10;
//                }
//                return result;
//            }
//        });
//
//        System.out.println("\nEven sums of digits:");
//        System.out.println(Arrays.toString(c));





        int[] d = Sequence.filter(arr, new ByCondition() {
            @Override
            public boolean isOk(int number) {

                // если в числе одна цифра
                // или рекурсивно обрабатывается последняя цифра в числе
                if (number / 10 == 0) {

                    // true если цифра не 0 и четная (младший бит 0)
                    return (number != 0) && (number & 1) != 1;
                }
                // если в числе не одна цифра
                // или рекурсивно обрабатывается не последняя цифра числа
                // то логически сравниваются младшие биты текущей цифры и следующей по разряду
                // для чего рекурсивно вызывается isOk()
                return ((number & 1) != 1) == isOk(number / 10);
            }
        });

        System.out.println("\nEven sums of digits array:");
        System.out.println(Arrays.toString(d));
    }





    // метод определения четности числа
    static boolean isEven ( int number){
        return (number & 1) != 1;
    }


}

