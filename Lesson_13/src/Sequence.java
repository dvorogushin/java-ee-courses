/**
 * Date: 07.11.2021
 * Project: Lesson_13
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class Sequence {

    /**
     * Метод по сути является итератором по элементам массива.
     * На каждой итерации проверяется соответствие элемента массива условиям, заданным в метод isOk().
     * Возвращает новый массив отобранных по критерию элементов.
     * Критерии отбора должны быть реализованы в методе isOk() класса, реализующего интерфейс ByCondition()
     * @param array - массив типа int
     * @param byCondition - объект интерфейса ByCondition(), реализующий метод isOk()
     * @return boolean, результат отбора по критерию
     */
    public static int[] filter(int[] array, ByCondition byCondition) {

        //определение количества элементов в массиве, удовлетворяющих условию метода isOk()
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            if (byCondition.isOk(array[i])) {
                count++;
            }
        }

        // формирование нового массива с элементами, удовлетворяющими условию метода isOk()
        int[] result = new int[count];
        count = 0;
        for (int i = 0; i < array.length; i++) {
            if (byCondition.isOk(array[i])) {
                result[count] = array[i];
                count++;
            }
        }
        return result;
    }
}
