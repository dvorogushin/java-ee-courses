import java.util.*;

public class Main {

    public static void main(String[] args) {
        // Разбираемый по словам текст.
        String text = "An object that maps keys to values.  A map cannot contain duplicate keys;\n" +
                " * each key can map to at most one value.\n" +
                " *\n" +
                " * <p>This interface takes the place of the {@code Dictionary} class, which\n" +
                " * was a totally abstract class rather than an interface.\n" +
                " *\n" +
                " * <p>The {@code Map} interface provides three <i>collection views</i>, which\n" +
                " * allow a map's contents to be viewed as a set of keys, collection of values,\n" +
                " * or set of key-value mappings.  The <i>order</i> of a map is defined as\n" +
                " * the order in which the iterators on the map's collection views return their\n" +
                " * elements.  Some map implementations, like the {@code TreeMap} class, make\n" +
                " * specific guarantees as to their order; others, like the {@code HashMap}\n" +
                " * class, do not.\n" +
                " *\n" +
                " * <p>Note: great care must be exercised if mutable objects are used as map\n" +
                " * keys.  The behavior of a map is not specified if the value of an object is\n" +
                " * changed in a manner that affects {@code equals} comparisons while the\n" +
                " * object is a key in the map.  A special case of this prohibition is that it\n" +
                " * is not permissible for a map to contain itself as a key.  While it is\n" +
                " * permissible for a map to contain itself as a value, extreme caution is\n" +
                " * advised: the {@code equals} and {@code hashCode} methods are no longer\n" +
                " * well defined on such a map.\n" +
                " *\n" +
                " * <p>All general-purpose map implementation classes should provide two\n" +
                " * \"standard\" constructors: a void (no arguments) constructor which creates an\n" +
                " * empty map, and a constructor with a single argument of type {@code Map},\n" +
                " * which creates a new map with the same key-value mappings as its argument.\n" +
                " * In effect, the latter constructor allows the user to copy any map,\n" +
                " * producing an equivalent map of the desired class.  There is no way to\n" +
                " * enforce this recommendation (as interfaces cannot contain constructors) but\n" +
                " * all of the general-purpose map implementations in the JDK comply.\n" +
                " *\n" +
                " * <p>The \"destructive\" methods contained in this interface, that is, the\n" +
                " * methods that modify the map on which they operate, are specified to throw\n" +
                " * {@code UnsupportedOperationException} if this map does not support the\n" +
                " * operation.  If this is the case, these methods may, but are not required\n" +
                " * to, throw an {@code UnsupportedOperationException} if the invocation would\n" +
                " * have no effect on the map.  For example, invoking the {@link #putAll(Map)}\n" +
                " * method on an unmodifiable map may, but is not required to, throw the\n" +
                " * exception if the map whose mappings are to be \"superimposed\" is empty.\n" +
                " *\n" +
                " * <p>Some map implementations have restrictions on the keys and values they\n" +
                " * may contain.  For example, some implementations prohibit null keys and\n" +
                " * values, and some have restrictions on the types of their keys.  Attempting\n" +
                " * to insert an ineligible key or value throws an unchecked exception,\n" +
                " * typically {@code NullPointerException} or {@code ClassCastException}.\n" +
                " * Attempting to query the presence of an ineligible key or value may throw an\n" +
                " * exception, or it may simply return false; some implementations will exhibit\n" +
                " * the former behavior and some will exhibit the latter.  More generally,\n" +
                " * attempting an operation on an ineligible key or value whose completion\n" +
                " * would not result in the insertion of an ineligible element into the map may\n" +
                " * throw an exception or it may succeed, at the option of the implementation.\n" +
                " * Such exceptions are marked as \"optional\" in the specification for this\n" +
                " * interface.\n" +
                " *\n" +
                " * <p>Many methods in Collections Framework interfaces are defined\n" +
                " * in terms of the {@link Object#equals(Object) equals} method.  For\n" +
                " * example, the specification for the {@link #containsKey(Object)\n" +
                " * containsKey(Object key)} method says: \"returns {@code true} if and\n" +
                " * only if this map contains a mapping for a key {@code k} such that\n" +
                " * {@code (key==null ? k==null : key.equals(k))}.\" This specification should\n" +
                " * <i>not</i> be construed to imply that invoking {@code Map.containsKey}\n" +
                " * with a non-null argument {@code key} will cause {@code key.equals(k)} to\n" +
                " * be invoked for any key {@code k}.  Implementations are free to\n" +
                " * implement optimizations whereby the {@code equals} invocation is avoided,\n" +
                " * for example, by first comparing the hash codes of the two keys.  (The\n" +
                " * {@link Object#hashCode()} specification guarantees that two objects with\n" +
                " * unequal hash codes cannot be equal.)  More generally, implementations of\n" +
                " * the various Collections Framework interfaces are free to take advantage of\n" +
                " * the specified behavior of underlying {@link Object} methods wherever the\n" +
                " * implementor deems it appropriate.\n" +
                " *\n" +
                " * <p>Some map operations which perform recursive traversal of the map may fail\n" +
                " * with an exception for self-referential instances where the map directly or\n" +
                " * indirectly contains itself. This includes the {@code clone()},\n" +
                " * {@code equals()}, {@code hashCode()} and {@code toString()} methods.\n" +
                " * Implementations may optionally handle the self-referential scenario, however\n" +
                " * most current implementations do not do so.\n" +
                " *";

        // С помощью метода split() разбиваем текст на слова.
        // В качестве шаблона для разделения используется регулярные выражения.
        // Разделяемые слова приводим к нижнему регистру.
        // Полученные слова складываем в массив.
        String[] splitText = text.toLowerCase().split("[?\"\n, .*<>():;@=/{}#-]+");

        // Проверка результата.
        System.out.println(Arrays.toString(splitText));

        // Формируем словарь, в котором Key - слово, Value - количество повторов.
        Map<String, Integer> map = new HashMap<>();

        // Цикл по массиву слов.
        for (String word : splitText) {

            // Если словарь уже содержит слово в качестве Key, то увеличиваем его Value на 1,
            // иначе добавляем в словарь новую пару
            if (map.containsKey(word)) {
                map.put(word, map.get(word) + 1);
            } else {
                map.put(word, 1);
            }
        }

        // Вывод результата на консоль.
        System.out.println(map.toString());
        System.out.println("Different words quantity: " + map.size());


        /**
         * Определение наиболее встречающегося слова
         */
        System.out.println("Most frequent word: ");
        int maxFreq = Collections.max(map.values());

        // Получение множества пар Key - Value
        Set<Map.Entry<String, Integer>> entries = map.entrySet();

        // Получение итератора
        Iterator<Map.Entry<String, Integer>> mapIterator = entries.iterator();

        while (mapIterator.hasNext()) {
            Map.Entry<String, Integer> currentEntry = mapIterator.next();
            if (currentEntry.getValue() == maxFreq) {
                System.out.println(currentEntry.toString());
            }
        }


        /**
         * Вывод 10 наиболее встречающихся слов
         */
        System.out.println("10 most frequent words:");
        int count = 10;                     // количество наиболее встречающихся слов

        // Получение НОВОЙ коллекции значений Value
        ArrayList<Integer> sortedFreq = new ArrayList<>(map.values());
        Collections.sort(sortedFreq);       // сортировка
        Collections.reverse(sortedFreq);    // изменение направления списка

        int prevFreq = 0;                   // хранение предыдущего значения частоты повторения слова
        for (Integer freq : sortedFreq) {   // цикл по частотам повторений слов
            if (prevFreq != freq && count > 0) {         // если текущую частоту уже обрабатывали
                for (Map.Entry<String, Integer> entry : entries) {      // вывод всех пар с указанной частотой
                    if (entry.getValue() == freq) {
                        System.out.println(entry);
                        count--;
                    }
                }
            }
            prevFreq = freq;

        }
    }
}
