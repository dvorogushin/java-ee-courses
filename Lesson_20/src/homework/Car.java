package homework;

/**
 * Date: 23.11.2021
 * Project: Lesson_20
 *
 * Класс описывает машину. Включает компаратор стоимости.
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class Car {
    private String number;
    private String model;
    private String color;
    private Integer mileage;
    private Integer price;

    public Car(String number, String model, String color, Integer mileage, Integer price) {
        this.number = number;
        this.model = model;
        this.color = color;
        this.mileage = mileage;
        this.price = price;
    }

    public String getNumber() {
        return number;
    }

    public String getModel() { return model; }

    public String getColor() {
        return color;
    }

    public Integer getMileage() {
        return mileage;
    }

    public Integer getPrice() {
        return price;
    }

    public static int priceCompare(Car a, Car b) {
        return a.getPrice() - (b.getPrice());
    }
}

