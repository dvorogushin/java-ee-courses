package homework;

import java.util.List;
import java.util.Map;

/**
 * Date: 23.11.2021
 * Project: Lesson_20
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class Main {
    public static void main(String[] args) {

        // получаем экземпляр каталога
        Catalog carCatalog = new Catalog("resources/cars.txt");
        // читаем данные из файла в коллекцию
        carCatalog.getCatalog();
        // запрос на список машин с определенным цветом и пробегом
        List<Car> list = carCatalog.getCarsByColorAndMileage("Black", 0);
        // запрос на список машин в диапазоне цен
        List<String> list1 = carCatalog.uniqModelsInPriceRange(100, 10000000);
        // получение машины с наименьшей стоимостью
        Car minPrice = carCatalog.getMinPrice();
        // получение средней цены по заданной модели
        double averagePrice = carCatalog.averagePriceByModel("Camry");
        // получение статистики по группам моделей
        Map<String, Double> averagePrices = carCatalog.averagePriceByModel();
        // вывод статистики на печать
        carCatalog.printStatistics();

        int i=0;

    }
}
