package homework;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Date: 23.11.2021
 * Project: Lesson_20
 * <p>
 * Класс представляет реализацию работы с набором данных (классом Car).
 * В классе производится чтение данных из файла и их обработка методами классов Stream и Collectors.
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class Catalog {

    // файл - источник данных
    private String filename;
    // список считанных объектов Car
    private List<Car> catalog;

    public Catalog(String filename) {
        this.filename = filename;
        this.catalog = new ArrayList<>();
    }

    /**
     * Чтение данных из файла. Преобразование к объекту Car.
     * Формирование списка объектов Car.
     *
     * @return true - если чтение данных прошло удачно и список не пустой.
     */
    public boolean getCatalog() {
        boolean result = false;
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            br.lines()
                    .map(s -> {
                        String[] lines = s.split("\\|");
                        return lines;
                    })
                    .map(line -> {
                        Car car = new Car(
                                line[0],
                                line[1],
                                line[2],
                                (int) Double.parseDouble(line[3]),
                                (int) Double.parseDouble(line[4]));
                        return car;
                    })
                    .forEach(catalog::add);
            result = !(catalog.isEmpty());
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
        return result;
    }

    /**
     * Получение списка машин заданного цвета и пробега.
     *
     * @param color   - цвет машины.
     * @param mileage - пробег машины.
     * @return List<Car> - список отфильтрованных машин.
     */
    public List<Car> getCarsByColorAndMileage(String color, Integer mileage) {
        return catalog.stream()
                .filter(car -> (car.getColor().equals(color) || (car.getMileage() == mileage)))
                .collect(Collectors.toList());
    }

    /**
     * Получение списка машин в заданном ценовом диапазоне.
     * Список состоит из уникальных моделей.
     *
     * @param from минимальная граница ценового диапазона.
     * @param to   максимальная граница ценового диапазона.
     * @return список уникальных моделей
     */
    public List<String> uniqModelsInPriceRange(int from, int to) {
        return catalog.stream()
                .filter(car -> ((car.getPrice() >= from) && (car.getPrice() <= to)))
                .map(Car::getModel)
                .distinct()
                .collect(Collectors.toList());
    }

    /**
     * Получение машины с минимальной стоимостью.
     *
     * @return
     */
    public Car getMinPrice() {
        return catalog.stream()
                .min(Car::priceCompare).get();
    }

    /**
     * Получение средней стоимости заданной модели машины.
     *
     * @param model - заданная модель.
     * @return double - средняя стоимость.
     */
    public double averagePriceByModel(String model) {
        long quantity = catalog.stream()
                .filter(car -> car.getModel().equals(model))
                .map(Car::getPrice)
                .count();

        Integer summ = catalog.stream()
                .filter(car -> car.getModel().equals(model))
                .map(Car::getPrice)
                .reduce(Integer::sum)
                .get();

        return (double) summ / quantity;
    }

    /**
     * Получение множества средней стоимости по каждой модели машины.
     *
     * @return Множество Модель-Ср.Стоимость
     */
    public Map<String, Double> averagePriceByModel() {
        return catalog.stream()
                .collect(Collectors.groupingBy(Car::getModel, Collectors.averagingInt(Car::getPrice)));
    }

    /**
     * Вывод на печать статистики по всему каталогу.
     * Группировка по модели машины.
     */
    public void printStatistics() {
        Map<String, IntSummaryStatistics> priceSummary =
                catalog.stream()
                        .collect(Collectors.groupingBy(Car::getModel, Collectors.summarizingInt(Car::getPrice)));

        System.out.println("Модель / Кол-во / Мин / Макс / Средн");
        for (Map.Entry<String, IntSummaryStatistics> item : priceSummary.entrySet()) {
            System.out.println(
                            item.getKey() + " / " +
                            item.getValue().getCount() + " / " +
                            item.getValue().getMin() + " / " +
                            item.getValue().getMax() + " / " +
                            item.getValue().getAverage());
        }
    }
}
