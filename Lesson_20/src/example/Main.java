package example;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Date: 23.11.2021
 * Project: Lesson_20
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class Main {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(10, 29, 47, 190);
//        Function<Integer, String> function = integer -> {
//            return integer.toString().replace("0", "Z");
//        };
        list.stream()
                .filter(number -> (number % 2) == 0)
//                .map(function);
                .map(number -> number.toString().replace("0", "Z"))
                .forEach(System.out::println);



        try (BufferedReader br = new BufferedReader(new FileReader("resources/test.txt"))) {
            br.lines().filter(s -> s.length() > 10).forEach(System.out::println);
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage()) ;
        }

    }
}
