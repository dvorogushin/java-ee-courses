import java.util.Optional;
import java.util.Scanner;

/**
 * Date: 28.11.2021
 * Project: Attestation01
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class Main {

    public static void main(String[] args) {

        // Создаем экземпляр класса, читающего текстовый файл с данными
        UsersRepository usersRepository = new UsersRepositoryFileImpl("resources/users.txt");
        // Вызываем метод, который читает файл построчно и создает внутри репозитория коллекцию для работы с User()
        if (usersRepository.readFile()) {
            System.out.println("Файл прочитан успешно!");
        } else {
            System.out.println("Файл пустой!");
        }

        // запрос Id пользователя, данные которого необходимо изменить
        System.out.print("Введите Id: ");
        Scanner scanner = new Scanner(System.in);
        String  id = scanner.nextLine();

        // Создаем экземпляр класса User, который будем изменять
        User user = null;
        // Получаем User() по id
        Optional<User> findUser = usersRepository.findById(id);
        if (findUser.isEmpty()) {
            System.out.printf("Пользователь c id {%s} не найден.%n", id);
        } else {
            user = findUser.get();
            System.out.printf("Пользователь найден.%n");
            System.out.println(user);
        }

        // заносим измененные данные User() в файл
        user.setName("Марсель");
        user.setAge(27);
        System.out.println(usersRepository.update(user));

    }
}
