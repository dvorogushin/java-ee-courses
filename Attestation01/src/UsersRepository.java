import java.util.Optional;

/**
 * Date: 28.11.2021
 * Project: Attestation01
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public interface UsersRepository {
    boolean update(User user);
    Optional<User> findById(String id);
    void print();
    boolean readFile();
}
