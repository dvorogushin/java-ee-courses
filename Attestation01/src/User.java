/**
 * Date: 28.11.2021
 * Project: Attestation01
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class User {
    private String  id;
    private String name;
    private int age;
    private boolean isWorker;

    public User(String  id, String name, int age, boolean isWorker) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.isWorker = isWorker;
    }

    public String  getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public boolean isWorker() {
        return isWorker;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setWorker(boolean worker) {
        isWorker = worker;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", isWorker=" + isWorker +
                '}';
    }
}
