import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Date: 28.11.2021
 * Project: Attestation01
 * <p>
 * Производит чтение и запись в/из файла filename.
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class UsersRepositoryFileImpl implements UsersRepository {

    private String filename;
    private List<User> repository;

    public UsersRepositoryFileImpl(String filename) {
        this.filename = filename;
        this.repository = new ArrayList<>();
    }

    /**
     * Читает файл построчно.
     * Данные заносятся в коллекцию repository.
     *
     * @return - true если коллекция не пустая.
     */
    @Override
    public boolean readFile() {
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            repository = br.lines()
                    .map(s -> {
                        String[] parts = s.split("\\|");
                        return parts;
                    })
                    .map(parts -> new User(
                            parts[0],
                            parts[1],
                            Integer.parseInt(parts[2]),
                            Boolean.parseBoolean(parts[3])
                    ))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
        return !repository.isEmpty();
    }

    /**
     * Ищет в коллекции repository объект User() по id.
     *
     * @param id идентификатор юзера в коллекции.
     * @return возвращает объект User(), обернутый в Optional.
     */
    @Override
    public Optional<User> findById(String id) {
        return repository.stream()
                .filter(user -> user.getId().equals(id))
                .findFirst();
    }

    /**
     * Меняет в файле строку с пользователем newUser.
     * Для этого:
     * - читает файл построчно, формирует коллекцию.
     * - в коллекции находит пользователя по id переданного newUser.
     * - меняет в коллекции данные пользователя на новые из newUser.
     * - записывает в файл измененную коллекцию построчно.
     *
     * @param newUser экземпляр User, которого нужно перезаписать в файле.
     * @return result - true пользователь найден и не были вызваны исключения ввода-вывода.
     */
    @Override
    public boolean update(User newUser) {
        boolean result = false;

        // читаем файл
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {

            //результат складываем в коллекцию
            List<String[]> list = br.lines()
                    .map(line -> {
                        String[] parts = line.split("\\|");
                        return parts;
                    })
                    .collect(Collectors.toList());

            // ищем в коллекции юзера по id
            String[] oldUser = list.stream()
                    .filter(strings -> strings[0].equals(newUser.getId()))
                    .findFirst().get();

            // меняем значения полей юзера на новые
            oldUser[1] = newUser.getName();
            oldUser[2] = String.valueOf(newUser.getAge());
            oldUser[3] = String.valueOf(newUser.isWorker());

            // перезаписываем всю коллекцию
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(filename))) {
                for (String[] str : list) {
                    bw.write(String.join("|", str[0], str[1], str[2], str[3]));
                    bw.newLine();
                }
                bw.flush();
                result = true;
            } catch (IOException e) {
                throw new IllegalArgumentException(e.getMessage());
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
        return result;
    }

    /**
     * Печать коллекции repository
     */
    public void print() {
        for (User user : repository) {
            System.out.println(user);
        }
    }
}
