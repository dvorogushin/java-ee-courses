import executors.TaskExecutor;
import executors.TaskExecutorThreadPool;
import executors.TaskExecutorWorkerThread;

import java.util.Scanner;

/**
 * Date: 26.11.2021
 * Project: task_threads
 * Чтение CSV файла построчно через CSVFileProcessor.
 * Инструкция на чтение с заданными аргументами и
 * получение результата производится в отдельном единственном треде,
 * созданном в TaskExecutorWorkedThread.
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */

public class MainThreadPool {

    public static void main(String[] args) {

        CSVFileProcessor processor = new CSVFileProcessor("resources/Data8317_cut.csv");

        long start = System.currentTimeMillis();

        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();

        /* Создается заданное количество потоков внутри TaskExecutorThreadPool,
        которые параллельно заглядывают во внутреннюю очередь с задачами. 4 потока - 11 сек.
        Экономия времени на сокращении операций открытия и закрытия потоков,
        то есть заданное количество тредов всегда запущены и находятся в wait() */
        TaskExecutor taskExecutor = new TaskExecutorThreadPool(6);

        taskExecutor.submit(() -> System.out.println(processor.countByPosition(0, "2018")));
        taskExecutor.submit(() -> System.out.println(processor.countByPosition(1, "045")));
        taskExecutor.submit(() -> System.out.println(processor.countByPosition(2, "1")));
        taskExecutor.submit(() -> System.out.println(processor.countByPosition(3, "2")));
        System.out.println("It takes: " + (System.currentTimeMillis() - start) / 1000);
    }
}
