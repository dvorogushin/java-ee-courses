import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Date: 26.11.2021
 * Project: task_threads
 * Чтение CSV файла построчно.
 * Результат - количество строчек в файле, содержащих заданный аргумент.
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class CSVFileProcessor {

    private String filename;

    public CSVFileProcessor(String filename) {
        this.filename = filename;
    }

    public long countByPosition(int position, String value) {
        long count = 1;
        try (BufferedReader bf = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = bf.readLine()) != null) {
                String[] parts = line.split(",");
                if (parts[position].equals(value)) {
                    count++;
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
        return count;
    }
}
