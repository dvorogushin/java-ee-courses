import executors.TaskExecutor;
import executors.TaskExecutorPerThread;
import java.util.Scanner;
/**
 * Date: 26.11.2021
 * Project: task_threads
 * Чтение CSV файла построчно через CSVFileProcessor.
 * Инструкция на чтение с заданными аргументами и
 * получение результата для каждой задачи производится в каждом отдельном треде.
 * @author Dmitry Vorogushin (@dvorogushin)
 */

public class MainPerTask {

    public static void main(String[] args) {

        CSVFileProcessor processor = new CSVFileProcessor("resources/Data8317_cut.csv");

        long start = System.currentTimeMillis();

        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();

        /* Последовательное выполнение чтения - 36 сек */
        System.out.println(processor.countByPosition(0, "2018"));
        System.out.println(processor.countByPosition(1, "045"));
        System.out.println(processor.countByPosition(2, "1"));
        System.out.println(processor.countByPosition(3, "2"));

        /* На каждую задачу создается свой тред - 11 сек.
        * Но все равно слишком затратно открывать и закрывать новый тред.*/
        TaskExecutor taskExecutor = new TaskExecutorPerThread();
        taskExecutor.submit(() -> {
            System.out.println(processor.countByPosition(0, "2018"));
        });
        taskExecutor.submit(() -> System.out.println(processor.countByPosition(1, "045")));
        taskExecutor.submit(() -> System.out.println(processor.countByPosition(2, "1")));
        taskExecutor.submit(() -> System.out.println(processor.countByPosition(3, "2")));

        System.out.println("It takes: " + (System.currentTimeMillis() - start) / 1000);
    }
}
