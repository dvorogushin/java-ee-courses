package executors;

import java.util.Deque;
import java.util.LinkedList;

/**
 * Date: 26.11.2021
 * Project: task_threads
 * Создается очередь задач и один бесконечный тред.
 * Задачи выполняются последовательно одним тредом.
 * Экономия времени на открытии и закрытии тредов.
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class TaskExecutorThreadPool implements TaskExecutor {

    // очередь задач FIFO
    private final Deque<Runnable> tasks;
    // массив потоков
    public final WorkerThread[] threads;

    // конструктор - создаем очередь задач,
    // и влож.классы - запущенные потоки
    public TaskExecutorThreadPool(int threadQuantity) {
        this.tasks = new LinkedList<>();
        this.threads = new WorkerThread[threadQuantity];
        for (Thread thread : threads) {
            thread = new WorkerThread();
            thread.start();
        }
    }

    // бесконечный тред, который выполняет все задачи из очереди
    private class WorkerThread extends Thread {
        @Override
        public void run() {
            System.out.format("\n %s запустился.", this.getName());
            while (true) {
                // задача, которую поток будет выполнять вне блока synchronized
                Runnable currentTask;
                // блокируем объект для безопасного удаления задачи из очереди в случае её наличия,
                // иначе уходим в wait()
                synchronized (tasks) {
                    // проверяем, если задачи в очереди задач
                    while (tasks.isEmpty()) {
                        System.out.format("\nCol is empty. %s waiting.", this.getName());
                        try {
                            // если задач нет, ждем уведомления
                            tasks.wait();
                        } catch (InterruptedException e) {
                            throw new IllegalArgumentException(e.getMessage());
                        }
                    }
                    // забираем с удалением задачу из начала очереди задач
                    System.out.printf("\n%s взял задачу.", this.getName());
                    currentTask = tasks.poll();
                }
                // запускаем задачу вне блока synchronized чтобы остальные потоки смогли заглянуть в tasks
                currentTask.run();
            }
        }
    }

    /**
     * Доьавление задачи в очередь задач
     * @param task
     */
    @Override
    public void submit(Runnable task) {
        // блокируем объект для безопасной работы с очередью
        synchronized (tasks) {
            // добавляем задачу в очередь
            tasks.add(task);
            // уведомляем о добавлении - фактически даем инструкцию выйти из wait
            tasks.notify();
        }
    }
}
