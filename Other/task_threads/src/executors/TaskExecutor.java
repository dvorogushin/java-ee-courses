package executors;

/**
 * Date: 26.11.2021
 * Project: task_threads
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public interface TaskExecutor {
    void submit(Runnable task);
}
