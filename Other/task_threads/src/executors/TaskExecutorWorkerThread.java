package executors;

import java.util.Deque;
import java.util.LinkedList;

/**
 * Date: 26.11.2021
 * Project: task_threads
 * Создается очередь задач и один бесконечный тред.
 * Задачи выполняются последовательно одним тредом.
 * Экономия времени на открытии и закрытии тредов.
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class TaskExecutorWorkerThread implements TaskExecutor {

    // очередь задач FIFO
    private final Deque<Runnable> tasks;
    /* поток, постоянно запущен,
    постоянно проверяет очередь,
    если не пустая, забирает задачу и вызывает run() */
    private final WorkerThread workerThread;

    // конструктор - создаем очередь задач,
    // и влож.класс - фактически постоянный поток
    public TaskExecutorWorkerThread() {
        this.tasks = new LinkedList<>();
        this.workerThread = new WorkerThread();
        workerThread.start();
    }

    // бесконечный тред, который выполняет все задачи из очереди
    private class WorkerThread extends Thread {
        @Override
        public void run() {
            while (true) {
                // блокируем объект для безопасной работы с очередью
                synchronized (tasks) {
                    // проверяем, если задачи в очереди задач
                    while (tasks.isEmpty()) {
                        try {
                            // если задач нет, ждем уведомления
                            tasks.wait();
                        } catch (InterruptedException e) {
                            throw new IllegalArgumentException(e.getMessage());
                        }
                    }
                    // забираем задачу из начала очереди задач
                    Runnable task = tasks.poll();
                    // run внутри run выполняет задачу внутри текущего потока
                    task.run();
                }
            }


        }
    }

    /**
     * Доьавление задачи в очередь задач
     * @param task
     */
    @Override
    public void submit(Runnable task) {
        // блокируем объект для безопасной работы с очередью
        synchronized (tasks) {
            // добавляем задачу в очередь
            tasks.add(task);
            // уведомляем о добавлении - фактически даем инструкцию выйти из wait
            tasks.notify();
        }
    }
}
