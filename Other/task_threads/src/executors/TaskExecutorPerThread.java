package executors;

/**
 * Date: 26.11.2021
 * Project: task_threads
 * На каждый созданный тред запускает run().
 * Назначение задач для каждого треда из MainPerTask().
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class TaskExecutorPerThread implements TaskExecutor {
    @Override
    public void submit(Runnable task) {
        Thread thread = new Thread(task);
        thread.start();
    }
}
