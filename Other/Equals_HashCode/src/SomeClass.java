public class SomeClass {
    int varA;
    int varB;

    @Override
    // простая реализация переопределения equals()
    public boolean equals(Object obj) {
        if (this == obj)                    // один и тот же объект?
            return true;
        if (obj == null)                    // не null?
            return false;
        if (getClass() != obj.getClass())   // совпадают ли классы?
            return false;
        SomeClass other = (SomeClass) obj;  // приведение obj к классу SomeClass()
        if (this.varA != other.varA)        // и сравнение переменных
            return false;
        if (this.varB != other.varB)
            return false;
        return true;
    }

    @Override
    // переопределение метода hashCode()
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + varA;
        result = prime * result + varB;
        return result;
    }

}
