public class Main {

    public static void main(String[] args) {

        Object obj = new Object();
        Object obj1 = new Object();
//        obj
        System.out.println(obj1.equals(obj)); // false

        obj1 = obj;

        System.out.println(obj.equals(obj)); // true

        Object obj2 = new Object();

        System.out.println(obj.hashCode()); // 1072591677
        System.out.println(obj.getClass()); // class java.lang.Object
        System.out.println(obj.toString()); // java.lang.Object@3fee733d

        System.out.println(obj1.hashCode()); // 1072591677
        System.out.println(obj1.getClass()); // class java.lang.Object
        System.out.println(obj1.toString()); // java.lang.Object@3fee733d

        System.out.println(obj2.hashCode()); // 1523554304
        System.out.println(obj2.getClass()); // class java.lang.Object
        System.out.println(obj2.toString()); // java.lang.Object@5acf9800

        String str = "A";
        System.out.println(str.hashCode());  // 65 - код ASCII

        Integer intr = 15;
        System.out.println(intr.hashCode()); // 15


    }

}
