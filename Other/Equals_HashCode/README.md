## Методы 'equals()' и 'hashCode()' и конракт между ними

* В ООП класс Object() входит в библиотеку java.lang и является суперклассом для каждого класса структуры проектов.
* В классе Object() определены 11 методов, в том числе методы 
  * equals()
  * hashCode()
  * toString()
  * getClass()
* Методы equals(), hashCode(), toString() могут быть переопределены для любого пользовательского класса наследника.
---
* Метод equals() в классе Object() по умолчанию имеет следующую реализацию:
  ```
  public boolean equals(Object obj) {
      return (this == obj);
  }
  ```
* Контракт equals():
  * рефлексивность: 
    - x.equals(x); // true
  * симметричность:
    - если    
      - x.equals(y); // true
    - то
      - y.equals(x); // true
  * транзитивность:
    - если 
      - x.equals(y); // true
      - y.equals(z); // true
    - то 
      - x.equals(z); // true
  * согласованность: 
    - повторный вызов x.equals(y) возвращает один и тот же результат
  * сравнение null:
    - x.equals(null); // false
* Для некоторых классов метод equals() имеет свою реализацию, например для String(), AbstractSet(), AbstractList(), AbstractMap() и пр.
* В классе String() метод equals() сравнивает строки посимвольно в лексографическом порядке.
* В классе AbstractList() метод equals() сравнивает коллекции по размеру, ключам и значениям.
----
* При создании объекта для него вычисляется хэш-код, это своего рода идентификатор объекта.
* По умолчанию для большинства объектов это число вычисляется по определенному алгоритму в зависимости от типа объекта, адреса объекта в памяти и пр. и имеет следующую реализацию:
  ```
  public native int hashCode();
  ```
* Хэш-код ограничен размером в 32 бита (тип int).
* Для некоторых классов метод hashCode() переопределен в зависимости от назначения класса:
  * для класса String():
      ```
      int hash = 0;
      for(int i = 0; i < length(); i++)
          hash = 31 * hash + charAt(i);
      ```
  * для класса Integer():
      ```
      public int hashCode() {
          return value;
      }
     ```
* Метод hashCode() так же может быть переопределен.
* Контракт между hashCode() и equals():
  * если
    * x.equals(y); // true
  * то
    * x.hashCode() == y.hashCode(); // true

* Источники:
  * https://coderoad.ru/2427631/%D0%9A%D0%B0%D0%BA-%D0%B2%D1%8B%D1%87%D0%B8%D1%81%D0%BB%D1%8F%D0%B5%D1%82%D1%81%D1%8F-hashCode-%D0%B2-Java
  * http://java-online.ru/hashcode.xhtml
  * https://javarush.ru/groups/posts/2179-metodih-equals--hashcode-praktika-ispoljhzovanija
  * https://javarush.ru/groups/posts/equals-java-sravnenie-strok
  * https://javarush.ru/groups/posts/1989-kontraktih-equals-i-hashcode-ili-kak-ono-vsje-tam
  * https://habr.com/ru/post/168195/
  * https://habr.com/ru/post/265373/
  * http://developer.alexanderklimov.ru/android/java/object.php
  * https://javarush.ru/groups/posts/1340-peregruzka-metodov-equals-i-hashcode-v-java
