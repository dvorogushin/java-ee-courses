public class Main {
    public static void main(String[] args) {

        int right;      // результат сдвига вправо
        int left;       // результат сдвига влево
        int rightPlus;  // результат беззнакового сдвига вправо

        for (int source = -3; source < 3; source++) {           // источник
            System.out.print("\n\n\t\t" + source + "\t\t\t");
            getFlags(source);

            left = source << 2;                                 // сдвиг влево на два бита
            System.out.print("\n <<2\t" + left + "\t\t\t");
            getFlags(left);

            right = source >> 2;                                // сдвиг вправо на два бита
            System.out.print("\n >>2\t" + right + "\t\t\t");
            getFlags(right);

            rightPlus = source >>> 2;                           // беззнаковый сдвиг вправо на два бита
            System.out.print("\n >>>2\t" + rightPlus + "\t\t\t");
            getFlags(rightPlus);
        }
    }
    // метод, также смещением читает и выводит флаги (биты) входного параметра
    static void getFlags(int source) {
        for (int flag = 31; flag >= 0; flag--) {                //цикл по битам
            // логическое AND входного параметра И числа со смещенным битом)
            if ((source & (1 << flag)) == (1 << flag)) {
                System.out.print(1);
            } else {
                System.out.print(0);
            }
        }
    }
}

