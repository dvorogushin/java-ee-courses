/**
 * Date: 24.11.2021
 * Project: Wait_notify
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class Consumer extends Thread {
    private String name;
    private Product product;

    public Consumer(Product product) {
        super("Consumer");
        this.name = "Consumer";
        this.product = product;
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            // локирует объект
            synchronized (product) {
                // если продукт готов
                if (product.isReady()) {
                    System.out.println(name + " использует продукт.");
                    product.consume(); // используем продукт
                    product.notify(); // через product уведомляем тех, у кого wait о необходимости проверки product
                } else {
                    System.out.println(name + " продукт не готов, ушел ждать.");
                    try {
                        product.wait(); // для бездействия
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
