/**
 * Date: 24.11.2021
 * Project: Wait_notify
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class Producer extends Thread {
    private String name;
    private Product product;

    public Producer(Product product) {
        super("Producer");
        this.name = "Producer";
        this.product = product;
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            // локирует product
            synchronized (product) {
                if (!product.isReady()) {
                    System.out.println(name + " готовит продукт.");
                    product.produce(); // готовит продукт
                    product.notify(); // уведомляет тех, у кого wait о необходимости проверки product
                } else {
                    System.out.println("Продукт готов. " + name + " ушел ждать.");
                    try {
                        product.wait(); // в случае бездействия
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

}
