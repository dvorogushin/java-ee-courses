/**
 * Date: 24.11.2021
 * Project: Wait_notify
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class Product {
    private boolean isReady;

    public Product(boolean isReady) {
        this.isReady = isReady;
    }

    public boolean isReady() {
        return isReady;
    }

    // употребление продукта
    public void consume() {
        isReady = false;
    }

    // производство продукта
    public void produce() {
        isReady = true;
    }
}


