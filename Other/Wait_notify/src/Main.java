/**
 * Реализована простая схема доступа двух тредов к одному объекту.
 * Поочередный доступ организован посредством одноместного семафора wait-notify.
 */
public class Main {

    public static void main(String[] args) {
	// write your code here
        Product product = new Product(true);
        Thread consumer = new Consumer(product);
        Thread producer = new Producer(product);

        consumer.start();
        producer.start();

    }
}
