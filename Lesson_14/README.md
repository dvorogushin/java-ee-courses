## В проекте [generics](src/generics) показано применение обобщенного класса [Cover()](src/generics/Cover.java).

## В проекте [nullable](src/nullable) показано применение аналога класса Optional() (в данном проекте [Nullable()](src/nullable/Nullable.java)).
