package nullable;

/**
 * Date: 09.11.2021
 * Project: Lesson_14
 *
 * Обобщенный утилитный класс, аналог Optional()
 * Служит для исключения возврата null при отсутствии объекта T
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class Nullable<T>{

    // поле с экземпляром Т
    private T value;

    // конструктор
    public Nullable(T value) {
        this.value = value;
    }

    // метод создания экземпляра Nullable
    public static  <T> Nullable <T> of (T value) {
        return new Nullable<>(value);
    }

    // проверка на наличие объекта Т
    public boolean isEmpty() {
        return value == null;
    }

    // возвращает объект Nullable() c null
    public static <T> Nullable<T> empty() {
        return new Nullable<>(null);
    }

    // возвращает объект Т
    public T get() {
        return value;
    }
}
