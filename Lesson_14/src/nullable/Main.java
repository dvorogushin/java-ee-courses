package nullable;

import java.util.Scanner;

/**
 * Date: 09.11.2021
 * Project: Lesson_14
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class Main {
    public static void main(String[] args) {

        // ввод почты и пароля из консоли
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter email: ");
        String email = scanner.nextLine();
        System.out.print("Enter password: ");
        String password = scanner.nextLine();

        // создаем объект UserService для вызова метода signIn()
        // в котором будет происходить проверка email и password
        UserService userService = new UserService();

        // в UserService возвращаем не сам объект User(),
        // а объект Nullable(), оборачивающий объект User() в случае его наличия
        // или пустой Nullable()
        Nullable <User> userNullable = userService.signIn(email, password);

        if (userNullable.isEmpty()) {
            System.err.println("Пользователь не найден.");
        } else {
            // получаем User() из Nullable()
            User user = userNullable.get();
            System.out.println("User: " + user.getEmail() + " " + user.getPassword() );
        }



    }
}
