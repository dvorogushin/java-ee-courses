package nullable;

/**
 * Date: 09.11.2021
 * Project: Lesson_14
 *
 * Класс авторизации пользователя
 * Для исключения возврата null в классе реализованно применение
 * класса Nullable() (аналог Optional())
 * То есть вместо возврата new User() возвращается Nullable.of(User())
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class UserService {
    public Nullable<User> signIn(String email, String password) {
        if (email.equals("dvorogushin@gmail.com") && password.equals("qwerty123")) {
            return Nullable.of(new User(email,password));
        } else {
            return Nullable.empty();
        }
    }
}
