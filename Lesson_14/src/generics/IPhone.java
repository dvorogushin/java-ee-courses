package generics;

/**
 * Date: 09.11.2021
 * Project: Lesson_14
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class IPhone {
    public void foto() {
        System.out.println("Iphone made foto!");
    }
}
