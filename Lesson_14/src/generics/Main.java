package generics;

public class Main {

    public static void main(String[] args) {

        Cover <Nokia3310> nokia3310Cover = new Cover();
        Nokia3310 nokia3310 = new Nokia3310();

        nokia3310Cover.setPhone(nokia3310);
        System.out.println(nokia3310Cover.getPhone());

        Nokia3310 nokiaFromCover = nokia3310Cover.getPhone();
        nokiaFromCover.call();

        Cover <IPhone> iPhoneCover   = new Cover();
        IPhone iPhone = new IPhone();

        iPhoneCover.setPhone(iPhone);
        IPhone iphoneFromCover = iPhoneCover.getPhone();
        iphoneFromCover.foto();


    }
}
