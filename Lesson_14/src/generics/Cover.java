package generics;

/**
 * Date: 09.11.2021
 * Project: Lesson_14
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class Cover<T> {
    private T phone;

    public T getPhone() {
        return phone;
    }

    public void setPhone(T phone) {
        this.phone = phone;
    }
}
