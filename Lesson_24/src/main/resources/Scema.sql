-- создание таблицы товаров
create table product
(
    id          serial primary key,
    description varchar,
    price       integer check ( price >= 0 ),
    quantity    integer check ( price >= 0 )
);

-- создание таблицы заказчиков
create table customer
(
    id   serial primary key,
    name varchar(255)
);

-- создание таблицы заказов
create table orderOf
(
    product_id  integer,
    foreign key (product_id) references product (id),
    customer_id integer,
    foreign key (customer_id) references customer (id),
    date        date,
    quantity    integer check (quantity >= 0)
);

-- заполнение таблицы товаров
insert into product (description, price, quantity)
VALUES ('Some description 1', 100, 1000);
insert into product (description, price, quantity)
VALUES ('Some description 2', 200, 2000);
insert into product (description, price, quantity)
VALUES ('Some description 3', 300, 3000);
insert into product (description, price, quantity)
VALUES ('Some description 4', 400, 4000);
insert into product (description, price, quantity)
VALUES ('Some description 5', 500, 5000);

-- заполнение таблицы заказчиков
insert into customer (name)
VALUES ('Customer 1');
insert into customer (name)
VALUES ('Customer 2');
insert into customer (name)
VALUES ('Customer 3');
insert into customer (name)
VALUES ('Customer 4');
insert into customer (name)
VALUES ('Customer 5');

-- заполнение таблицы заказов
insert into orderOf (product_id, customer_id, date, quantity)
VALUES (1, 5, '1-1-2021', 300);
insert into orderOf (product_id, customer_id, date, quantity)
VALUES (2, 5, '1-2-2021', 150);
insert into orderOf (product_id, customer_id, date, quantity)
VALUES (5, 4, '1-3-2021', 800);
insert into orderOf (product_id, customer_id, date, quantity)
VALUES (4, 1, '1-4-2021', 1000);
insert into orderOf (product_id, customer_id, date, quantity)
VALUES (3, 3, '1-5-2021', 200);
insert into orderOf (product_id, customer_id, date, quantity)
VALUES (1, 3, '1-6-2021', 3000);
insert into orderOf (product_id, customer_id, date, quantity)
VALUES (4, 4, '1-7-2021', 800);
insert into orderOf (product_id, customer_id, date, quantity)
VALUES (1, 1, '1-8-2021', 1000);
insert into orderOf (product_id, customer_id, date, quantity)
VALUES (3, 5, '1-8-2021', 200);
insert into orderOf (product_id, customer_id, date, quantity)
VALUES (5, 5, '1-10-2021', 2000);
insert into orderOf (product_id, customer_id, date, quantity)
VALUES (1, 2, '1-11-2021', 50);
insert into orderOf (product_id, customer_id, date, quantity)
VALUES (1, 1, '1-12-2021', 50);

--количство заказов, произведеных заказчиком 3
select count(*)
from orderOf
where customer_id = 3;

-- сколько сделал заказов каждый заказчик
select name,
       (select count(*)
        from orderOf
        where customer_id = customer.id)
from customer;

-- сумма заказов и сумма стоимости заказов по каждому продукту в порядке возврастания суммы стоимости заказов
select description,
       (select sum(quantity)
        from orderOf
        where orderOf.product_id = product.id)
           as sum_order,
       ((select sum(quantity)
         from orderOf
         where orderOf.product_id = product.id) * price)
           as sum_price
from product
order by sum_price;

-- максимальный и минимальный заказы по стоимости заказа по продукту
select description,
       ((select MIN(quantity)
         from orderOf
         where orderOf.product_id = product.id) * price)
           as min_sum,
       ((select MAX(quantity)
         from orderOf
         where orderOf.product_id = product.id) * price)
           as max_sum
from product;

