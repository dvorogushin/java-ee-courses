import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        // инициализация класса Logger()
        Logger logger = Logger.getInstance();

        //вызов метода log(), который записывает сообщения в Logger() через каждые 3 секунды
        logger.log("Message1");     TimeUnit.SECONDS.sleep(3);
        logger.log("Message2");     TimeUnit.SECONDS.sleep(3);
        logger.log("Message3");     TimeUnit.SECONDS.sleep(3);
        logger.log("Message4");

        // получение другой ссылки на единственный экземпляр синглтона
        Logger logger1 = Logger.getInstance();

        // вывод в консоль всех сообщений
        logger1.log();
    }
}
