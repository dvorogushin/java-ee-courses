/**
 * Синглтон, принимает, хранит и выводит в консоль текстовые сообщения
 */

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Logger {

    // поле, хранящее единственный экземпляр синглтона
    private static final Logger instance;

    // статический инициализатор
    static {
        instance = new Logger();
    }

    //
    /**
     * метод получения единственного экземпляра синглтона
     * @return instance - экземпляр класса
     */
    public static Logger getInstance() {
        return instance;
    }

    // приватный конструктор, инициализация массивов
    private Logger() {
        this.messages = new String[MAX_MESSAGE_COUNT];      // принимаемые сообщения
        this.now = new LocalDateTime[MAX_MESSAGE_COUNT];    // время приема сообщения
    }

    //максимальное количество сообщений
    private final static int MAX_MESSAGE_COUNT = 100;

    // поля класса
    private String[] messages;
    private int count;
    private LocalDateTime[] now;

    /**
     * Метод принимает сообщения в виде переменных типа String,
     * заносит в массив текст сообщения и время обращения
     * @param message - принимаемое сообщение
     */
    public void log(String message) {
        if (count < MAX_MESSAGE_COUNT) {
            this.messages[count] = message;
            this.now[count] = LocalDateTime.now();
            count++;
            System.out.println("Message #" + count + " added into logger.");
            System.out.println("Left: " + (MAX_MESSAGE_COUNT - count));
        } else {
            System.err.println("Logger is full!");
        }
    }

    /**
     * Вывод на консоль всех внесенных сообщений
     */
    public void log() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        for (int i = 0; i < count; i++){
            System.out.println(this.now[i].format(formatter) + " Message #" + count + ": " + messages[i]);
        }
    }


}
