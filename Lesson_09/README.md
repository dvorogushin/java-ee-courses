## Наследование 

* В проекте реализован механизм наследования, инкапсуляции и полиморфизма.

* В проекте создан класс [Figure()](src/figures/Figure.java), с полями x, y, соответствующими координатам фигуры, и методами .paint() и getPerimeter().

* От класса Figure() наследуются два класса [Ellipse()](src/figures/Ellipse.java) и [Rectangle()](src/figures/Rectangle.java), от которых, в свою очередь, наследуются классы [Circle()](src/figures/Circle.java) и [Square()](src/figures/Square.java).

* В каждом классе наследнике переопределяются (@override) методы .paint() и .getPerimeter().
 
 