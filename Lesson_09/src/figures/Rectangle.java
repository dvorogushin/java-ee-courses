package figures;

public class Rectangle extends Figure {
    private double side1;
    private double side2;

    //getter for side1
    public double getSide1() {
        return side1;
    }

    //setter for side1
    public void setSide1(double side1) {
        if (side1 < 0) {
            side1 *= -1;
        }
        this.side1 = side1;
    }

    //getter for side2
    public double getSide2() {
        return side2;
    }

    //setter for side2
    public void setSide2(double side2) {
        if (side2 < 0) {
            side2 *= -1;
        }
        this.side2 = side2;
    }

    //full constructor
    public Rectangle(int x, int y, double side1, double side2) {
        super(x, y);
        setSide1(side1);
        setSide2(side2);
    }

    //constructor with coordinates, without sides
    public Rectangle(int x, int y) {
        super(x, y);
        setSide1(0);
        setSide2(0);
    }

    //constructor with sides, without coordinates
    public Rectangle(double side1, double side2) {
        super(0, 0);
        setSide1(side1);
        setSide2(side2);
    }

    //method paint
    public void paint() {
        System.out.println("Figure: Rectangle.");
        System.out.println("Coordinates: " + this.x + ", " + this.y);
        System.out.println("Side1: " + this.getSide1() + ", side2: " + this.getSide2());
    }

    //method getPerimeter
    public double getPerimeter() {
        return (side1 + side2) * 2;
    }

}
