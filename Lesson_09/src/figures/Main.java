package figures;

public class Main {

    public static void main(String[] args) {

        Figure[] figures = new Figure[5];
        figures[0] = new Figure(4,5);
        figures[1] = new Ellipse(10.0, 20.0);
        figures[2] = new Rectangle(10.0, 15.0);
        figures[3] = new Circle(45);
        figures[4] = new Square(16);

        for (Figure fig:figures) {
            fig.paint();
            System.out.println("Perimeter: " + String.format("%,.1f",fig.getPerimeter()));
            System.out.println();
        }
    }
}
