package figures;

public class Square extends Rectangle {

    //constructor with only one side
    public Square(double side1) {
        super(side1, side1);
    }

    //constructor with only coordinates
    public Square(int x, int y) {
        super(x, y);
    }

    //constructor with coordinates and one side
    public Square(int x, int y, double side1) {
        super(x, y, side1, side1);
    }

    //paint method
    public void paint() {
        System.out.println("Figure: Square.");
        System.out.println("Coordinates: " + this.x + ", " + this.y);
        System.out.println("Side: " + this.getSide1());
    }


}
