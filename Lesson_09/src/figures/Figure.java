package figures;

class Figure {
    protected int x;
    protected int y;

    public Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }

    double getPerimeter() {
        return 0;
    }

    void paint() {
        System.out.println("Figure.");
        System.out.println("Coordinates: " + this.x + ", y: " + this.y);
    }
}
