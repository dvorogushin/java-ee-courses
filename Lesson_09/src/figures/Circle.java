package figures;

public class Circle extends Ellipse {

    //full constructor
    public Circle(int x, int y, double radius1) {
        super(x, y, radius1, radius1);
    }

    //constructor with radius only
    public Circle(double radius1) {
        super(radius1, radius1);
    }

    //constructor with cordinates only
    public Circle(int x, int y) {
        super(x, y);
    }

    //paint method
    public void paint() {
        System.out.println("Figure: Circle.");
        System.out.println("Coordinates: " + this.x + ", " + this.y);
        System.out.println("Radius: " + this.radius1);
    }

    //getPerimeter method
    public double getPerimeter() {
        return 2 * Math.PI * this.radius1;
    }


}
