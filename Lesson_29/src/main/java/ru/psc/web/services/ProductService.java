package ru.psc.web.services;

import ru.psc.web.forms.ProductForm;
import ru.psc.web.models.Product;

import java.util.List;

/**
 * Date: 08.12.2021
 * Project: Lesson_29
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */

public interface ProductService {

    void addProduct(ProductForm productForm);

    List<Product> getAllProducts();

    void deleteProductById(Integer productId);

    Product getProduct(Integer productId);


    void updateProduct(ProductForm productForm);
}
