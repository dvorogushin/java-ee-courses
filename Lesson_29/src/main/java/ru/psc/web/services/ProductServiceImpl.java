package ru.psc.web.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.psc.web.forms.ProductForm;
import ru.psc.web.models.Product;
import ru.psc.web.repositories.ProductsRepository;

import java.util.List;

/**
 * Date: 08.12.2021
 * Project: Lesson_29
 * <p>
 * Отвечает за взаимодействие между репозиторием c контроллером
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */

@Component
public class ProductServiceImpl implements ProductService {

    // экземпляр репозитория
    private final ProductsRepository productsRepository;

    @Autowired
    public ProductServiceImpl(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    // получение товара из запроса, отправка товара в репозиторий
    @Override
    public void addProduct(ProductForm productForm) {
        Product product = Product.builder()
                .description(productForm.getDescription())
                .price(productForm.getPrice())
                .quantity(productForm.getQuantity())
                .build();
        productsRepository.save(product);

    }

    // получение из репозитория всех товаров
    @Override
    public List<Product> getAllProducts() {
        List<Product> products = productsRepository.findAll();
        return products;
    }

    // вызов метода удаления товара из БД через репозиторий
    @Override
    public void deleteProductById(Integer productId) {
        productsRepository.deleteProduct(productId);
    }

    // получение из репозитория данных по конкретному товару
    @Override
    public Product getProduct(Integer productId) {
        Product product = productsRepository.findProductById(productId);
        return product;
    }

    // получение данных из запроса, вызов метода репозитория на изменение данных по товару
    @Override
    public void updateProduct(ProductForm productForm) {
        Product product = Product.builder()
                .id(productForm.getId())
                .description(productForm.getDescription())
                .price(productForm.getPrice())
                .quantity(productForm.getQuantity())
                .build();
        productsRepository.updateProduct(product);
    }
}
