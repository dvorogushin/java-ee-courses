package ru.psc.web.forms;

import lombok.Data;

/**
 * Date: 08.12.2021
 * Project: Lesson_29
 * Шаблон Product для полученных данных в запросе
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Data
public class ProductForm {

    // конструктор для преобразования полученных данных из String
    public ProductForm(String id, String description, String price, String quantity) {
        this.id = Integer.getInteger(id);
        this.description = description;
        this.price = Double.parseDouble(price);
        this.quantity = Integer.getInteger(quantity);
    }

    private Integer id;
    private String description;
    private Double price;
    private Integer quantity;
}
