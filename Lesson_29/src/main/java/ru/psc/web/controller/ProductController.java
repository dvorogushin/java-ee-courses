package ru.psc.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.psc.web.forms.ProductForm;
import ru.psc.web.models.Product;
import ru.psc.web.services.ProductService;

import java.rmi.MarshalledObject;
import java.util.List;

/**
 * Date: 06.12.2021
 * Project: Spring-Boot
 * Обработка запросов, вызов соответствующих методов Service,
 * Формирование данных для динамических страниц, перенаправление на соответствующие страницы
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Controller
public class ProductController {

    // экземпляр Service
    private final ProductService productService;

    // конструктор для Service
    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    // запрос на отображение всех товаров
    @GetMapping("/url_products")
    public String getProducts(Model model) {
        List<Product> products = productService.getAllProducts();
        model.addAttribute("productsAttr", products);
        return "products";
    }

    // запрос на добавление товара
    @PostMapping("/url_products_add")
    public String addProduct(ProductForm productForm) {
        productService.addProduct(productForm);
        return "redirect:/url_products";
    }

    // запрос на удаление товара
    @PostMapping("/products/{product-id}/delete")
    public String deleteProduct(@PathVariable("product-id") Integer productId) {
        productService.deleteProductById(productId);
        System.out.println("Norm");
        return "redirect:/url_products";
    }

    // запрос на страницу заданного товара
    @GetMapping("/products/{product-id}")
    public String getProductPage(Model model, @PathVariable("product-id") Integer productId) {
        Product product = productService.getProduct(productId);
        model.addAttribute("product", product);
        return "/product";
    }

    // запрос на изменение товара
    @PostMapping("/product/{product-id}/update")
    public String updateProduct(ProductForm productForm) {
        productService.updateProduct(productForm);
        return "redirect:/url_products";
    }
}
