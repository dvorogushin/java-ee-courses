package ru.psc.web.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Date: 06.12.2021
 * Project: Lesson_28
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Product {
    private Integer id;
    private String description;
    private Double price;
    private Integer quantity;
}
