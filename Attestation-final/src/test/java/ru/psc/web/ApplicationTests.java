package ru.psc.web;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
/**
 * Date: 06.12.2021
 * Project: Lesson_28
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */

@SpringBootTest
class ApplicationTests {

    @Test
    void contextLoads() {
    }

}
