package ru.psc.web.forms;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Date: 11.12.2021
 * Project: Attestation-final
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Getter
@Setter
@AllArgsConstructor
public class SignUpForm {

    private String name;
    private String address;
    private String email;
    private String password;
}
