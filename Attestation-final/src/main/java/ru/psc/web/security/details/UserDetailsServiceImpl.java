package ru.psc.web.security.details;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.psc.web.models.Customer;
import ru.psc.web.repositories.CustomerRepository;

import java.util.Optional;

/**
 * Date: 13.12.2021
 * Project: Attestation-final
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */

@RequiredArgsConstructor
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final CustomerRepository customerRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
//        Optional<Customer> customer = customerRepository.findByEmail(email);
//        System.out.println(customer.get());
        return new UserDetailsImpl(customerRepository
                .findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("User not found")));
    }
}
