package ru.psc.web.models;

import lombok.*;

import javax.persistence.*;
import java.util.List;

/**
 * Date: 09.12.2021
 * Project: Attestation-final
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Customer {

    public enum Role {
        USER, ADMIN
    }


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String email;
    private String hashPassword;

    @Enumerated(value = EnumType.STRING)
    private Role role;

    private String name;
    private String address;

    @OneToMany (mappedBy = "customer")
    private List<Order> orders;

}
