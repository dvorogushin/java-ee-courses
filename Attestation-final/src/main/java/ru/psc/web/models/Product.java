package ru.psc.web.models;

import lombok.*;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import javax.persistence.*;
import java.util.List;

/**
 * Date: 06.12.2021
 * Project: Lesson_28
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String description;
    private Double price;
    private Integer quantity;

    @OneToMany(mappedBy = "product")
    private List<Order> orders;


}
