package ru.psc.web.models;

import lombok.*;

import javax.persistence.*;
import java.sql.Date;

/**
 * Date: 09.12.2021
 * Project: Attestation-final
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "order_list")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Date orderDate;
    private Date paymentDate;
    private Integer quantity;
    private Integer sum;

    @ManyToOne
    @JoinColumn(name= "product_id")
    private Product product;

    @ManyToOne
    @JoinColumn(name= "customer_id")
    private Customer customer;
}
