package ru.psc.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.psc.web.forms.SignUpForm;
import ru.psc.web.services.SignUpService;

/**
 * Date: 11.12.2021
 * Project: Attestation-final
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@RequiredArgsConstructor
@Controller
@RequestMapping("/signup") // все запросы к /signup
public class SignUpController {

    private final SignUpService signUpService;

    @GetMapping
    public String getSignUpPage(Authentication auth) {
        if (auth != null) {
            return "/";
        }
        return "signup";
    }

    @PostMapping
    public String signUpCustomer(SignUpForm form) {
        signUpService.signUpUser(form);
        return "redirect:/signin";
    }


}
