package ru.psc.web.controller;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Date: 13.12.2021
 * Project: Attestation-final
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Controller
@RequestMapping("/signin")
public class SignInController {

    @GetMapping
    public String getSignPage(Authentication auth) {
        if (auth != null) {
            return "/";
        }
        return "/signin";
    }
}
