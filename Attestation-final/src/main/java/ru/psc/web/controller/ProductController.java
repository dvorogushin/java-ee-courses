package ru.psc.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.psc.web.forms.ProductForm;
import ru.psc.web.models.Customer;
import ru.psc.web.models.Product;
import ru.psc.web.services.CustomerService;
import ru.psc.web.services.ProductService;

import java.util.List;

/**
 * Date: 06.12.2021
 * Project: Spring-Boot
 * Обработка запросов, вызов соответствующих методов Service,
 * Формирование данных для динамических страниц, перенаправление на соответствующие страницы
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@RequiredArgsConstructor
@Controller
public class ProductController {

    // экземпляр Service
    private final ProductService productService;
    private final CustomerService customerService;

    // страница каталога
    @GetMapping("/catalog")
    public String getProducts(Model model) {
        List<Product> products = productService.getAllProducts();
        model.addAttribute("productsAttr", products);
        return "products";
    }

    // страница каталога
    @GetMapping("/")
    public String getProductsAlso(Model model) {
        List<Product> products = productService.getAllProducts();
        model.addAttribute("productsAttr", products);
        return "products";
    }

    // страница товара
    @GetMapping("/products/{product-id}")
    public String getProduct(Model model, @PathVariable("product-id") Long productId) {
        Product product = productService.getProduct(productId);
        model.addAttribute("product", product);
        return "product";
    }

    @GetMapping("/products/add")
    public String getProductAdd(Model model) {
        List<Product> products = productService.getAllProducts();
        model.addAttribute("products", products);
        return "product_add";
    }

    @PostMapping("/products/add")
    public String addProduct(ProductForm form, Model model) {
        productService.addProduct(form);
        List<Product> products = productService.getAllProducts();
        model.addAttribute("products", products);
        return "product_add";
    }

}
