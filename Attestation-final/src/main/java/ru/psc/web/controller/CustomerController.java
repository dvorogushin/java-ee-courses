package ru.psc.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.psc.web.models.Customer;
import ru.psc.web.services.CustomerService;

/**
 * Date: 10.12.2021
 * Project: Attestation-final
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */

@RequiredArgsConstructor
@Controller
public class CustomerController {

    // экземпляр Service
// private final ProductService productService;
    private final CustomerService customerService;

    // страница заказчика
    @GetMapping("/customer")
    public String getCustomer(Model model,
                              @AuthenticationPrincipal(expression = "id") Long userId) {
        Customer customer = customerService.getCustomer(userId);
        model.addAttribute("customer", customer);
        return "customer";
    }

    // страница заказчика + добавление товара в корзину
    @PostMapping("/customer")
    public String addToBacket(Model model,
                              @AuthenticationPrincipal(expression = "id") Long userId,
                              @RequestParam("productId") Long productId) {
        customerService.createOrder(userId, productId);
        Customer customer = customerService.getCustomer(userId);
        model.addAttribute("customer", customer);
        return "customer";
    }

    @PostMapping("/customer/pay")
    public String payOrder(Model model,
                             @AuthenticationPrincipal(expression = "id") Long userId,
                             @RequestParam("orderId") Long orderId) {
        customerService.payOrder(userId, orderId);
        Customer customer = customerService.getCustomer(userId);
        model.addAttribute("customer", customer);
        return "redirect:/customer";
    }

    @PostMapping("/customer/delete")
    public String cancelOrder(Model model,
                             @AuthenticationPrincipal(expression = "id") Long userId,
                             @RequestParam("orderId") Long orderId,
                             @RequestParam("productId") Long productId) {
        customerService.cancelOrder(productId, orderId);
        Customer customer = customerService.getCustomer(userId);
        model.addAttribute("customer", customer);
        return "redirect:/customer";
    }
}
