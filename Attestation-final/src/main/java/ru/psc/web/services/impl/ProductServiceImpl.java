package ru.psc.web.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import ru.psc.web.forms.ProductForm;
import ru.psc.web.models.Customer;
import ru.psc.web.models.Product;
import ru.psc.web.repositories.CustomerRepository;
import ru.psc.web.repositories.ProductRepository;
import ru.psc.web.services.ProductService;

import java.util.List;

/**
 * Date: 09.12.2021
 * Project: Attestation-final
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */

@RequiredArgsConstructor
@Component
public class ProductServiceImpl implements ProductService {

private final ProductRepository productRepository;

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAll(Sort.by(Sort.Direction.ASC, "description"));
    }

    @Override
    public Product getProduct(Long productId) {
        return productRepository.getById(productId);
    }

    @Override
    public void addProduct(ProductForm form) {
        Product product = Product.builder()
                .name(form.getName())
                .description(form.getDescription())
                .price(form.getPrice())
                .quantity(form.getQuantity())
                .build();
        productRepository.save(product);
    }

}
