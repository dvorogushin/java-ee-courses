package ru.psc.web.services;

import ru.psc.web.models.Customer;
import ru.psc.web.models.Order;

/**
 * Date: 09.12.2021
 * Project: Attestation-final
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */

public interface CustomerService {
    Customer getCustomer(Long customerId);

    void payOrder(Long customerId, Long orderId);

    void createOrder(Long customerId, Long productId);

    void cancelOrder(Long productId, Long orderId);
}
