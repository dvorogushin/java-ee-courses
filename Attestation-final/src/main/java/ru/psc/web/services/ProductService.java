package ru.psc.web.services;

import ru.psc.web.forms.ProductForm;
import ru.psc.web.models.Customer;
import ru.psc.web.models.Product;

import java.util.List;

/**
 * Date: 08.12.2021
 * Project: Lesson_29
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */

public interface ProductService {

   List<Product> getAllProducts();

    Product getProduct(Long productId);

    void addProduct(ProductForm form);
}
