package ru.psc.web.services;

import ru.psc.web.forms.SignUpForm;

/**
 * Date: 11.12.2021
 * Project: Attestation-final
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */

public interface SignUpService {

    void signUpUser(SignUpForm form);
}
