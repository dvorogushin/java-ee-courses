package ru.psc.web.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.psc.web.models.Customer;
import ru.psc.web.models.Order;
import ru.psc.web.models.Product;
import ru.psc.web.repositories.CustomerRepository;
import ru.psc.web.repositories.OrderRepository;
import ru.psc.web.repositories.ProductRepository;
import ru.psc.web.services.CustomerService;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

/**
 * Date: 09.12.2021
 * Project: Attestation-final
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@RequiredArgsConstructor
@Component
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;
    private final OrderRepository orderRepository;
    private final ProductRepository productRepository;

    @Override
    public Customer getCustomer(Long customerId) {
        Customer customer = customerRepository.getById(customerId);
        List<Order> orders = orderRepository.findAllByCustomer_IdOrderByIdAsc(customerId);
        customer.setOrders(orders);
        return customer;
    }

    @Override
    public void payOrder(Long customerId, Long orderId) {
        Order order = orderRepository.getById(orderId);
        order.setPaymentDate(Date.valueOf(LocalDate.now()));
        orderRepository.save(order);
    }

    @Override
    public void createOrder(Long customerId, Long productId) {
        Customer customer = customerRepository.getById(customerId);
        Product product = productRepository.getById(productId);
        Order order = Order.builder()
                .orderDate(Date.valueOf(LocalDate.now()))
                .quantity(1)
                .sum(product.getPrice().intValue())
                .product(product)
                .customer(customer)
                .build();
        orderRepository.save(order);
        product.setQuantity(product.getQuantity() - 1);
        productRepository.save(product);
    }

    @Override
    public void cancelOrder(Long productId, Long orderId) {
        Product product = productRepository.getById(productId);
        product.setQuantity(product.getQuantity() + 1);
        productRepository.save(product);
        Order order = orderRepository.getById(orderId);
        orderRepository.delete(order);
    }


}
