package ru.psc.web.services.impl;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.psc.web.forms.SignUpForm;
import ru.psc.web.models.Customer;
import ru.psc.web.repositories.CustomerRepository;
import ru.psc.web.services.SignUpService;

/**
 * Date: 11.12.2021
 * Project: Attestation-final
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@RequiredArgsConstructor
@Component
public class SignUpServiceImpl implements SignUpService {

    private final PasswordEncoder passwordEncoder;

    private final CustomerRepository customerRepository;

    @Override
    public void signUpUser(SignUpForm form) {
        Customer customer = Customer.builder()
                .name(form.getName())
                .address(form.getAddress())
                .email(form.getEmail())
                .hashPassword(passwordEncoder.encode(form.getPassword()))
                .role(Customer.Role.USER)
                .build();
        customerRepository.save(customer);
    }
}
