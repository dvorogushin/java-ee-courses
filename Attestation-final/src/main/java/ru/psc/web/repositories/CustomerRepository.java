package ru.psc.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.psc.web.models.Customer;

import java.util.Optional;

/**
 * Date: 09.12.2021
 * Project: Attestation-final
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    Optional<Customer> findByEmail(String email);

}
