package ru.psc.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.psc.web.models.Customer;
import ru.psc.web.models.Order;

import java.util.List;

/**
 * Date: 10.12.2021
 * Project: Attestation-final
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */

public interface OrderRepository extends JpaRepository<Order, Long> {
    List<Order> findAllByCustomer_IdOrderByIdAsc(Long id);
}
