package ru.psc.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.psc.web.models.Product;

import java.util.List;

/**
 * Date: 09.12.2021
 * Project: Attestation-final
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */

public interface ProductRepository extends JpaRepository<Product, Long> {
}
