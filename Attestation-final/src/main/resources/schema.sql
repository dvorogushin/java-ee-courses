-- создание таблицы товаров
create table product
(
    id          serial primary key,
    name        varchar(255),
    description varchar,
    price       DOUBLE PRECISION check ( price >= 0 ),
    quantity    integer check (quantity >= 0 )
);

-- заполнение таблицы товаров
insert into product (name, description, price, quantity)
VALUES ('Компьютер', 'Компьютер большой Intel', 50000, 10);
insert into product (name, description, price, quantity)
VALUES ('Компьютер', 'Компьютер маленький Intel', 40000, 10);
insert into product (name, description, price, quantity)
VALUES ('Компьютер', 'Компьютер большой Mac', 60000, 10);
insert into product (name, description, price, quantity)
VALUES ('Компьютер', 'Компьютер малый Mac', 45000, 10);
insert into product (name, description, price, quantity)
VALUES ('Ноутбук', 'Ноутбук большой Mac', 50000, 10);
insert into product (name, description, price, quantity)
VALUES ('Ноутбук', 'Ноутбук малый Mac', 65000, 10);
insert into product (name, description, price, quantity)
VALUES ('Ноутбук', 'Ноутбук большой Intel', 75000, 10);
insert into product (name, description, price, quantity)
VALUES ('Ноутбук', 'Ноутбук малый Intel', 85000, 10);

-- создание таблицы заказчиков
create table customer
(
    id            bigserial primary key,
    email         varchar unique,
    hash_password varchar,
    name          varchar(255),
    description   varchar
);

-- заполнение таблицы заказчиков
insert into customer (email, hash_password, name, address)
VALUES ('Империя-техно', 'г.Москва, ул.Незабываемая, 28.');
insert into customer (email, hash_password, name, address)
VALUES ('Компьютерра', 'г.Санкт-Петербург, ул.Фрунзе, 10.');
insert into customer (email, hash_password, name, address)
VALUES ('Сити', 'г.Сургут, ул.Ленина, 65.');
insert into customer (email, hash_password, name, address)
VALUES ('Рога и копыта', 'г.Владикавказ, ул.Хачика, д.б/н');

-- создание таблицы заказов
create table order_list
(
    id           serial primary key,
    order_date   date,
    payment_date date,
    quantity     integer check (quantity >= 0),
    sum          integer,
    product_id   bigint not null,
    foreign key (product_id) references product (id),
    customer_id  bigint not null,
    foreign key (customer_id) references customer (id)
);

-- заполнение таблицы заказов
insert into order_list (order_date, payment_date, quantity, sum, product_id, customer_id)
VALUES ('1-10-2021', '2-10-2021', 100, 1000, 1, 2);
insert into order_list (order_date, payment_date, quantity, sum, product_id, customer_id)
VALUES ('2-10-2021', '3-10-2021', 200, 2000, 2, 3);
insert into order_list (order_date, quantity, sum, product_id, customer_id)
VALUES ('1-10-2021', 300, 3000, 3, 2);
