//import Human;

public class Main {

    public static void main(String[] args) {

        // инициализация массива объектов
        Human[] humans = new Human[10];

        String[] names = {"Vasiliy", "Dmitry", "Alexey", "Veronika", "Mariana", "Fedor", "Maxim", "Galina", "Egor", "Artem"};

        // инициализация экземпляров объекта
        // присваивание веса рандомно
        for (int i = 0; i < humans.length; i++){
            humans[i] = new Human();
            humans[i].setName(names[i]);
            humans[i].setWeight(60 + ((int) (Math.random() * 60)));
        }

        // вывод входящего массива объектов
        System.out.println("Входящий массив объектов:");
        for ( int i = 0; i < humans.length; i++){
            System.out.println("Name[" + i + "]: " + humans[i].getName() + ", Weight[" + i + "]: " + humans[i].getWeight());
        }

        System.out.println("\nОтсортированный массив объектов по весу:");

        // временный экземпляр
        Human tmp = new Human();

        //индекс экземпляра с минимальным весом
        int indexMinWeight;
        //цикл в цикле для сортировки
        for (int j = 0; j < humans.length; j++) {
            indexMinWeight = j;
            for (int i = j; i < humans.length; i++) {
                //запоминаем минимальный вес в каждом проходе
                if (humans[indexMinWeight].getWeight() > humans[i].getWeight()) {
                    indexMinWeight = i;
                }
            }
            // перестановка элементов в массиве если j-ый элемент не минимальный
            if (indexMinWeight != j) {
                tmp = humans[j];
                humans[j] = humans[indexMinWeight];
                humans[indexMinWeight] = tmp;
            }
            System.out.println("Name[" + j + "]: " + humans[j].getName() + ", Weight[" + j + "]: " + humans[j].getWeight());
        }
    }
}
