package ru.psc.web.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ru.psc.web.models.Product;

import javax.sql.DataSource;
import java.util.List;

/**
 * Date: 06.12.2021
 * Project: Lesson_28
 *
 * Сервис, отвечающий за отправку запросов к базе данных, плучение ответов,
 * формирование результата в виде списка.
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Component
public class ProductsRepositoryJdbcTemplateImpl implements ru.psc.web.repositories.ProductsRepository {


    // запрос на добавление записи в таблицу
    //language=SQL
    private static final String SQL_INSERT = "INSERT into product(description, price, quantity) VALUES (?, ?, ?)";

    // запрос на получение всех товаров из таблицы product
    //language=SQL
    private static final String SQL_SELECT_ALL = "SELECT * FROM product ORDER BY id";

    // запрос на получение товаров с определенной ценой
    //language=SQL
    private static final String SQL_FIND_BY_PRICE = "SELECT * FROM product WHERE price = ? ORDER BY id";

    // запрос на получение товаров, которые встречются в таблице заказов orderOf заданное количество раз
    //language=SQL
    private static final String SQL_FIND_BY_ORDERS_COUNT =
                    "SELECT p.id, p.description, p.price, p.quantity, count(*) " +
                    "FROM orderOf ord left join product p on p.id =ord.product_id " +
                    "GROUP BY p.id " +
                    "HAVING count(*) = ?";

    // отвечает за отправку запросов и получение ответов из СУБД
    @Autowired
    private JdbcTemplate jdbcTemplate;

    // конструктор, создающий экземляр JdbcTemplate
    public ProductsRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    // RowMapper - шаблон формирования экземпляра Product из строки таблицы product
    public static final RowMapper<Product> productRowMapper = (row, rowNumber) -> {
        Integer id = row.getInt("id");
        String description = row.getString("description");
        Double price = row.getDouble("price");
        Integer quantity = row.getInt("quantity");
        return new Product(id, description, price, quantity);
    };

    // заносит данные в БД
    @Override
    public void save(Product product) {
        jdbcTemplate.update(SQL_INSERT, product.getDescription(), product.getPrice(), product.getQuantity());
    }

    // читает таблицу product, возвращает все строки из таблицы
    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, productRowMapper);
    }

    // читает таблицу product, возвращает лист с товарами заданной цены price
    @Override
    public List<Product> findAllByPrice(double price) {
        return jdbcTemplate.query(SQL_FIND_BY_PRICE, productRowMapper, price);
    }

    // читает таблицу заказов orderOf,
    // возвращает список товаров, которые встречаются в таблице заказов
    // заданное (ordersCount) количество раз
    @Override
    public List<Product> findAllByOrdersCount(int ordersCount) {
        return jdbcTemplate.query(SQL_FIND_BY_ORDERS_COUNT, productRowMapper, ordersCount);
    }
}
