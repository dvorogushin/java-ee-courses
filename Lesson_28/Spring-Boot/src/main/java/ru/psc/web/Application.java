package ru.psc.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Date: 06.12.2021
 * Project: Lesson_28
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
