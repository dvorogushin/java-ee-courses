package ru.psc.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.psc.web.models.Product;
import ru.psc.web.repositories.ProductsRepository;

/**
 * Date: 06.12.2021
 * Project: Spring-Boot
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Controller
public class ProductController {

    // автоматическое связывание с @Component
    @Autowired
    private ProductsRepository productsRepository;

    @PostMapping("/products")
    public String addProduct(@RequestParam("description") String description,
                             @RequestParam("price") Double price,
                             @RequestParam("quantity") Integer quantity) {

        Product product = Product.builder()
                .description(description)
                .price(price)
                .quantity(quantity)
                .build();
        productsRepository.save(product);

        return "redirect:/products_add.html";
    }
}
