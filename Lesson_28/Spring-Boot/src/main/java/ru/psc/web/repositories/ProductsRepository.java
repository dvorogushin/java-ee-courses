package ru.psc.web.repositories;

import ru.psc.web.models.Product;

import java.util.List;

/**
 * Date: 06.12.2021
 * Project: Lesson_28
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */

public interface ProductsRepository {

    void save(Product product);

    List<Product> findAll();

    List<Product> findAllByPrice(double price);

    List<Product> findAllByOrdersCount(int ordersCount);
}
