package dataInputOutputStream;

import java.io.Serializable;

/**
 * Date: 22.11.2021
 * Project: Metanit
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class Person implements Serializable {
    private String name;
    private int age;
    private double height;
    private boolean married;

    // не сериализуется - transient
    private transient boolean hasChildren;

    public Person(String name, int age, double height, boolean married) {
        this.name = name;
        this.age = age;
        this.height = height;
        this.married = married;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public double getHeight() {
        return height;
    }

    public boolean isMarried() {
        return married;
    }
}
