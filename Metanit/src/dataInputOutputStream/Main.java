package dataInputOutputStream;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Date: 22.11.2021
 * Project: Metanit
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class Main {
    public static void main(String[] args) {
        List<Person> people = new ArrayList<>();
        people.add(new Person("Dmitriy", 40, 183, false ));
        people.add(new Person("Aleksey", 40, 182, true));
        try (
                ObjectOutputStream oos = new ObjectOutputStream(
                        new FileOutputStream("src/dataInputOutputStream/people.dat"))) {
            // Object should implement Serializable interface
            oos.writeObject(people);
            System.out.println("File has been written.");
        } catch (IOException e) {
            System.err.println("Во время записи что-то пошло не так: " + e.getMessage());
        }

        List<Person> newPeople = new ArrayList<>();
        try (
                ObjectInputStream ois = new ObjectInputStream(
                        new FileInputStream("src/dataInputOutputStream/people.dat"))) {
            newPeople = (ArrayList<Person>) ois.readObject();
            System.out.println("File has been read.");
        } catch (IOException |
                ClassNotFoundException e) {
            System.err.println("Во время чтения что-то пошло не так: " + e.getMessage());
        }
        for (
                Person p : newPeople) {
            System.out.println(p.getName());
        }
    }
}
