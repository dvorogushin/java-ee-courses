package fileReader;

import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        // write your code here
        try (FileReader reader = new FileReader("src/fileReader/notes.txt")) {
            char[] buf = new char[256];
            // можно без while, если известно, что размер файла будет меньше buf:
//            int c = reader.read(buf);
//            System.out.print(buf);

            // считываем символы сразу размером buf и в buf
            int c;
            while ((c = reader.read(buf)) > 0) {
                if (c < 256) {
                    buf = Arrays.copyOf(buf, c);
                }
                System.out.print(buf);
            }
        } catch (IOException e) {
            System.err.println("File not found: " + e.getMessage());
        }
    }
}
