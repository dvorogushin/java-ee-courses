package bufferedRead_bufferedWrite;

import java.io.*;

/**
 * Date: 22.11.2021
 * Project: Metanit
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class Main {
    public static void main(String[] args) {

        try (
                // открываем два потока: считывающий с консоли и записывающий в файл
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                BufferedWriter bw = new BufferedWriter(new FileWriter("src/bufferedRead_bufferedWrite/notes.txt"))) {
            String text;
            while (!(text = br.readLine()).equals("")) {
                bw.write(text + "\n");
                bw.flush();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }


    }
}
