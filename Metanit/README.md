### [fileReader](src/fileReader/Main.java) - считывается текстовый файл в массив-буфер char[] определенным размером. 
### [bufferedRead_bufferedWrite](src/bufferedRead_bufferedWrite/Main.java) - считывает с консоли построчно и записывает в файл.
### [dataInputOutputStream](src/dataInputOutputStream/Main.java) - сериализует (записывает) в файл и десериализует (считывает) коллекцию с объектами.