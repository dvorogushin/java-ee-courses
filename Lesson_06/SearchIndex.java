import java.util.Scanner;

public class SearchIndex {

    /**
     * Возвращает индекс заданного числа в массиве
     * @param массив, в котором производится поиск числа
     * @param число, индекс которого требуется найти 
     * @return индекс числа, либо (-1)
     */

  
    public static int getIndex(int[] array, int searchNumber) {
        int ind = -1;
        
        for (int i = 0; i < array.length; i++) {

            if (array[i] == searchNumber) {
                ind = i;
                break; //первый найденный индекс
            }
        }
        
        return ind;
    }


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        
        System.out.print ("Введите размер массива (1-128): ");
        int arrLen = scanner.nextInt();
        int arr[] = new int[arrLen];

        System.out.println("Массив из случайных чисел: ");
        
        //формирование массива из случ.чисел до 128
        for (int i = 0; i < arrLen; i++) {

            arr[i] = (int) (Math.random () * 128);
            System.out.print(arr[i] + " ");
        }

        System.out.print ("\nВведите число, индекс которого требуется найти: ");
        int searchNumber = scanner.nextInt();
        
        //непосредственно вызов функции
        int result = getIndex(arr, searchNumber);

        if (result == -1) {
            System.out.println("Введенное число (" + searchNumber + ") в массиве не найдено.");
        }
        else {
            System.out.println("Индекс первого найденного числа (" + searchNumber + ") в массиве: " + result);
        }
    }
}
