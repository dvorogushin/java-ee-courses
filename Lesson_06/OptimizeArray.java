import java.util.Scanner;

public class OptimizeArray {

    /**
     * Преобразовывает массив, перемещая значимые числа (>0) в левую часть
     * @param массив, в котором производится перемещение значимых чисел влево
     * @return 
     */

    //процедура преобразования массива
    public static void optArray(int[] array) {
        
        //индекс самого первого (левого) значения "0"
        //при вхождении в цикл условно принимается как последний элемент массива
        int firstEmtpyIndex =  array.length;
        
        for (int i = 0; i < array.length; i++) {
            
            //первый найденный "0" в цикле
            if (array[i] == 0 && firstEmtpyIndex == array.length) {
                firstEmtpyIndex = i;
            }
            
            //перемещение значимого числа на позицию с найденным "0"
            if (array[i] != 0 && firstEmtpyIndex < i) {
                array[firstEmtpyIndex] = array[i];
                array[i] = 0;
                firstEmtpyIndex++;
            }
        }
    }
        


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        
        System.out.print ("Введите размер массива (1-128): ");
        int arrLen = scanner.nextInt();
        int arr[] = new int[arrLen];

        System.out.println("Массив из случайных чисел: ");
        
        //присваивание случайных чисел
        for (int i = 0; i < arrLen; i++) {

            //половина нули, другая половина - случайные числа
            if ((int) (Math.random() * 2) == 1) {
                arr[i] = (int) (Math.random () * 128);
            }
            else {
                arr[i] = 0;
            }

            System.out.print(arr[i] + " ");
        }

        //непосредственно вызов процедуры
        optArray(arr);

        System.out.println("\nПреобразованный массив: ");

        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }   
        System.out.println();

    }
}
