## Программа поиска числа в массиве
### из случайных чисел

* ![SearchIndex.java](SearchIndex.java)

![Результат](SearchIndex.jpg)

## Программа оптимизации массива
### Перемещение вещественных чисел влево

* ![OptimizeArray.java](OptimizeArray.java)

![Результат](OptimizeArray.jpg)

