## В проекте [arrayList](src/arrayList) представлен класс [ArrayList()](src/arrayList/ArrayList.java), созданный на основе массива.
### В данном классе реализованы все основные методы класса java.util.ArrayList<T>:
* **add(**_value_**)**
* **add(**_index, value_**)**
* **get(**_index_**)**
* **set(**_index_**)**
* **remove(**_index_**)**
* **clear()**
* **contains(**_value_**)**
* **isEmpty()**
* **size()**
* **toArray()**

## В проекте [linkedList](src/linkedList) представлен класс [LinkedList()](src/linkedList/LinkedList.java), который представляет собой упрощенную реализацию двусвязного списка.
### В данном классе реализованы следующие методы:
* **add(**_value_**)** - добавление узла в конец списка;
* **addToBegin(**_value_**)** - добавление узла в начало списка;
* **get(**_index_**)** - получение значения узла по индексу узла;
* **remove(**_value_**)** - удаление узлов со значением value;
* **remove(**_oldValue, newValue_**)** - замена всех значений oldValue на newValue
* **isContain(**_value_**)** - проверка на наличие узлов со значением value;
* 