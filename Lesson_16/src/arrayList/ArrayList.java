package arrayList;

/**
 * Date: 11.11.2021
 * Project: Lesson_16
 *
 * Коллекция на основе массива.
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class ArrayList<T> {

    private static final int DEFAULT_SIZE = 10;
    private T[] elements;
    private int size;

    public ArrayList() {
        this.elements = (T[]) new Object[DEFAULT_SIZE];
        this.size = 0;
    }

    /**
     * Добавляет элемент в конец списка
     * @param element - добавляемый элемент типа Т
     */
    public void add(T element) {
        if (isFullArray()) {
            resizeArray();
        }
        elements[size] = element;
        size++;

        System.out.println("Element \'" + element + "\' was added to the end of array.");
        print();
    }

    /**
     * Увеличивает размер массива на 50%, который содержит список
     */
    private void resizeArray() {
        T[] oldArray = elements;
        elements = (T[]) new Object[(int)(elements.length + (elements.length >> 1))];
        System.arraycopy(oldArray, 0, elements, 0,oldArray.length);
    }

    /**
     * Проверяет достижение предельного размера массива
     * @return
     */
    private boolean isFullArray() {
        return size == elements.length;
    }

    /**
     * Возвращает элемент списка по индексу
     * @param index индекс элемента, который требуется вернуть
     * @return возвращаемый элемент или null
     */
    public T get(int index) {
        if (isInRange(index)) {
            return elements[index];
        } else {
            System.err.println("Index out of range.");
            return null;
        }
    }

    /**
     * Вывод списка в консоль
     */
    public void print(){
        System.out.print("Size: " + size + " Array: ");
        for (int i = 0; i < size; i++) {
            System.out.print(elements[i] + " ");
        }
        System.out.println();
    }

    /**
     * Проверка индекса на допустимые значения
     * @param index
     * @return
     */
    private boolean isInRange(int index) {
        return index >= 0 && index < size;
    }

    /**
     * Замена элемента в списке по индексу
     * @param index индекс заменяемого элемента
     * @param element новый элемент
     */
    public void set(int index, T element) {
        if (isInRange(index)){
            this.elements[index] = element;
            System.out.println("Element with position " + index + " was changed on \'" + element + "\'.");
            print();
        } else {
            System.err.println("Index out of range.");
        }
    }

    /**
     * Получение размера списка
     * @return
     */
    public int size() {
        return size;
    }

    /**
     * Добавление элемента в список в заданную позицию.
     * Все элементы выше по списку смещаются.
     * Размер списка увеличивается на 1.
     * @param index
     * @param element
     */
    public void add(int index, T element) {
        if (isInRange(index)) {
            if (isFullArray()) {
                resizeArray();
            }
            System.arraycopy(elements, index, elements, index +1, size - index);
            this.elements[index] = element;
            size++;
            System.out.println("Element \'" + element + "\' added into " + index + " position.");
            print();
        } else {
            System.err.println("Index out of range.");
        }
    }

    /**
     * Удаление элемента из списка.
     * Все элементы выше по индексу смещаются вниз на 1 позицию.
     * Размер списка уменьшается на 1.
     * @param index
     */
    public void remove(int index) {
        if (isInRange(index)) {
            System.arraycopy(elements, index + 1, elements, index, size - index + 1);
            size--;
            System.out.println("Element on position " + index + " was removed from array.");
            print();
        } else {
            System.err.println("Index out of range.");
        }
    }

    /**
     * Очистка списка.
     * Если размер массива превышал DEFAULT_SIZE - создается новый массив размером DEFAULT_SIZE
     */
    public void clear() {
        this.size = 0;
        if (this.elements.length > DEFAULT_SIZE) {
            this.elements = (T[]) new Object[DEFAULT_SIZE];
        }
        System.out.println("Array was cleared.");
        print();
    }

    /**
     * Проверка на наличие элемента в списке.
     * @param element
     * @return
     */
    public boolean contains(T element) {
        for (int i = 0; i < size; i++) {
            if (this.elements[i].equals(element)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Проверка, не является ли список пустым.
     * @return
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Принимает массив и заполняет элементами списка.
     * Если размер принимаемого массива меньше размера списка, то лишние элементы игнорируются.
     * Если размер принимаемого массива или размер списка равен нулю, то возвращается null.
     * @param arr принимаемый массив
     * @return возвращаемый массив
     */
    public T[] toArray(T[] arr) {
        int loopSize = Math.min(arr.length, size);
        if (loopSize != 0) {
            System.arraycopy(elements, 0, arr, 0, loopSize);
            return arr;
        }
        System.err.println("Array size or collection size equals zero.");
        return null;
    }
}
