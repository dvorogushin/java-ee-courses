package arrayList;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
	// write your code here
        ArrayList<String> list = new ArrayList<>();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        list.add("6");
        list.add("7");
        list.add("8");
        list.add("9");
        list.add("10");
        list.set(5, "rrr");
        list.add(8,"321");
        list.add(3,"t");
        list.remove(3);
        list.remove(10);

        System.out.println("Contains 321 - " + list.contains("321"));

        System.out.println("Is empty - " + list.isEmpty());
//        list.clear();
        System.out.println("Is empty - " + list.isEmpty());
        list.print();

        System.out.println("Removed to array:");
        String[] s = list.toArray(new String[list.size()]);
        System.out.println(Arrays.toString(s));

    }
}
