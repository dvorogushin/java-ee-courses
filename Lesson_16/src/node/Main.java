package node;

/**
 * Date: 12.11.2021
 * Project: Lesson_16
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class Main {
    public static void main(String[] args) {
        Node<String> a = new Node<>("Marsel1");
        Node<String> b = new Node<>("Marsel2");
        Node<String> c = new Node<>("Marsel3");
        Node<String> d = new Node<>("Marsel4");
        Node<String> e = new Node<>("Marsel5");

        a.setNext(b);
        b.setNext(c);
        c.setNext(d);
        d.setNext(e);
        e.setNext(null);

        Node<String> current = a;
        while (current != null) {
            System.out.println(current.getValue());
            current = current.getNext();
        }




        int i = 0;
    }
}
