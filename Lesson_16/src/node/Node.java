package node;

/**
 * Date: 12.11.2021
 * Project: Lesson_16
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class Node<T> {
    private T value;
    private Node<T> next;

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public Node<T> getNext() {
        return next;
    }

    public void setNext(Node<T> next) {
        this.next = next;
    }

    public Node(T value) {
        this.value = value;
    }
}
