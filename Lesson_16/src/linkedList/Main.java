package linkedList;

/**
 * Date: 12.11.2021
 * Project: Lesson_16
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class Main {
    public static void main(String[] args) {

        LinkedList<String> list = new LinkedList<>();
        list.addToBegin("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        list.add("6");
        list.add("8");
        list.add("7");
        list.add("8");
        list.add("8");

        list.addToBegin("0");
        list.addToBegin("0");
        list.addToBegin("8");

        list.remove("2");
        list.remove("0");
        list.remove("8");

        System.out.println(list.isContain("5"));;
        System.out.println(list.isContain(""));;

        list.remove("5", "rrr");

        System.out.println(list.isContain("rrr"));;

        System.out.println(list.get(5));
        System.out.println(list.get(0));
        System.out.println(list.get(20));

        int i = 0;
    }
}
