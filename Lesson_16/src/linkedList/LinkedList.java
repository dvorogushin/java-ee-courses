package linkedList;

/**
 * Date: 12.11.2021
 * Project: Lesson_16
 *
 * Реализация двухсвязного списка.
 * Значения каждого узла и ссылки на следующий и предыдущий узлы хранятся в классе Node().
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class LinkedList<T> {

    /**
     * Класс хранит значение (<T>) узла в переменной value, а также ссылки на следующий (next) и предыдущий (prev) узлы.
     * @param <T>
     */
    private static class Node<T> {
        private T value;        // значение, хранимое в узле

        private Node<T> next;   // ссылка на предыдущий узел
        private Node<T> prev;   // ссылка на следующий узел

        private Node(T value) {
            this.value = value;
        }

    }
    private int size;       // размер списка
    private Node<T> first;  // первый элемент списка
    private Node<T> last;   // последний элемент списка

    /**
     * Возвращает значение поля n-го узла.
     * @param index - индекс узла, значение которого необходимо вернуть.
     * @return - возвращаемое значение узла типа Т.
     */
    public T get(int index) {
        if (index < size && index >= 0) {       // проверка на соответствие запрашиваемого узла размеру списка
            Node<T> current = first;            // временный узел для перемещения по списку
            int currentIndex = 0;               // индекс временного узла
            while (currentIndex != index) {     // цикл по узлам списка
                current = current.next;
                currentIndex++;
            }
            return current.value;
        } else {
            System.err.println("Index out of Range.");
            return null;
        }
    }

    /**
     * Добавление нового узла в конец списка.
     * @param value - принимаемое значение типа Т нового узла.
     */
    public void add(T value) {
        Node<T> newNode = new Node<T>(value);
        if (size == 0) {
            first = newNode;
        } else {
            last.next = newNode;
            newNode.prev = last;
        }
        last = newNode;
        size++;
    }

    /**
     * Добавление нового узла в начало списка.
     * @param value - принимаемое значение типа Т нового узла.
     */
    public void addToBegin(T value) {
        Node<T> newNode = new Node<T>(value);
        if (size != 0) {
            first.prev = newNode;
            newNode.next = first;
            first = newNode;
            size++;
        } else {
            add(value);
        }
    }

    /**
     * Проверка на наличие в списке узла со значением поля value, соответствующего искомому значению.
     * @param value - искомое значение узла.
     * @return
     */
    public boolean isContain(T value) {
        Node<T> current = first;
        while (current != null) {
            if (current.value.equals(value)) {
                return true;
            }
            current = current.next;
        }
        return false;
    }

    /**
     * Удаление всех узлов со значением поля value, соответствующим значению принимаемого параметра value.
     * @param value - значение поля удаляемого узла.
     */
    public void remove(T value) {
        Node<T> current = first;                    // временный узел для перемещения по узлам списка
        while (current != null) {
            if (current.value.equals(value)) {
                if (current == first) {             // если необходимо удалить первый узел
                    current.next.prev = null;
                    first = current.next;
                } else if (current == last) {       // если необходимо удалить последний узел
                    current.prev.next = null;
                    last = current.prev;
                } else {                            // если необходимо удалить не первый и не последний узел
                    current.prev.next = current.next;
                    current.next.prev = current.prev;
                }
                size--;
            }
            current = current.next;                 // перемещение на один узел вперед
        }
    }

    /**
     * Заменяет все значения поля oldValue на newValue.
     * @param oldValue - искомое значение.
     * @param newValue - новое значение.
     */
    public void remove(T oldValue, T newValue) {
        Node<T> current = first;
        while (current != null) {
            if (current.value.equals(oldValue)) {
                current.value = newValue;
            }
            current = current.next;
        }
    }
}
