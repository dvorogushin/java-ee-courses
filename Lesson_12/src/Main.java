public class Main {

    public static void main(String[] args) {

        // создание экземпляра словаря и добавление двух пар key - value
        Table table = new Table();
        table.put("Georgy", 35);
        table.put("Mark", 30);

        //создание экземпляра внутреннего класса для операций со словарем
        Table.Operations operations = table.new Operations();

        System.out.println(operations.getKeys());


    }
}
