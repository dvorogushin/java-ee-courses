/**
 * Date: 05.11.2021
 * Project: Lesson_12
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
// outer class
public class Table {

    /**
     *  Вложенный класс (nested), обязательное наличие модификатора static
     *  Хранит пары key - value
     */
    static class Entry {
        private int value;  // напр. 35
        private String key; // напр. "Georgy"

        /**
         * Конструктор. Вызывается внутри метода put внешнего класса.
         * Функционально - добавление новой пары в словарь.
         * @param key
         * @param value
         */
        private Entry(String key, int value) {
            this.value = value;
            this.key = key;
        }
    }

    /**
     * Операции манипуляций с массивом объектов Entry() вынесены в отдельный внутренний (inner) класс.
     */
    public class Operations {

        public String getKeys() {
            String result = "";
            for (int i = 0; i < count; i++) {
                result += entries[i].key + " ";
            }
            return result;
        }
    }

    /**
     * Поля внешнего класса.
     * Хранят информацию об экземпляре класса и массив экземпляров класса Entry(), хотя static
     */
    private Entry entries[];            // массив с экземплярами класса Entry()
    private int count;                  // счётчик количества инициализированных экземпляров класса Entry()
    private static int MAX_VALUE = 10;

    /**
     * Конструктор.
     * При инициализации создаётся массив ссылок на объекты Entry(), хранящие пары key - value
     */
    public Table(){
        entries = new Entry[MAX_VALUE];
        count = 0;
    }

    /**
     * Метод инициализации очередного объекта Entry(), хранящего пару key-value
     * @param key
     * @param value
     */
    public void put(String key, int value){

        // проверка на наличие дубликата key
        for (int i = 0; i < count; i++) {
            // если значение key уже существует, то перезаписываем value
            if (this.entries[i].key.equals(key)) {
                this.entries[i].value = value;
                return;
            }
        }
        // проверка на переполнение
        if (count < MAX_VALUE) {
            // создание экземпляра класса с парами key - value
            this.entries[count] = new Entry(key, value);
            count++;
        } else {
            System.err.println("Dictionary is full.");
        }
    }
}
