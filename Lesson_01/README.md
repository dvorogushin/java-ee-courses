# Вычитание отрицательных чисел

## Задание: 121(10)-68(10)=53(10)

### Перевод числа 121 из десятичной в двоичную систему счисления:

```
	121 / 2 | 1	- младший бит (last significant bit)
	 60 / 2 | 0
	 30 / 2 | 0
	 15 / 2 | 1
	  7 / 2 | 1
	  3 / 2 | 1
		  1
		  0	- старший бит (most significant bit)(*8)
	
	121(10) = 01111001(2)
```
Проверка:
```
			0 * 2 ^ 7 = 0 * 128 	= 0
		+	1 * 2 ^ 6 = 1 * 64	= 64
		+	1 * 2 ^ 5 = 1 * 32	= 32
		+ 	1 * 2 ^ 4 = 1 * 16 	= 16
		+ 	1 * 2 ^ 3 = 1 * 8	= 8
		+ 	0 * 2 ^ 2 = 0 * 4	= 0
		+ 	0 * 2 ^ 1 = 0 * 2 	= 0
		+ 	1 * 2 ^ 0 = 1 * 1 	= 1
		____________________________
		64 + 32 + 16 + 8 + 1 = 121
```

### Перевод числа 68 из десятичной в двоичную систему счисления
```
	68 / 2 | 0
	34 / 2 | 0
	17 / 2 | 1
	 8 / 2 | 0
	 4 / 2 | 0
	 2 / 2 | 0
		 1
		 0

	68(10) = 01000100(2)
```
Проверка:
```
			|0| * 2 ^ 7 = |0| * 128 = 0
		+	|1| * 2 ^ 6 = |1| * 64	= 64
		+	|0| * 2 ^ 5 = |0| * 32	= 0
		+ 	|0| * 2 ^ 4 = |0| * 16 	= 0
		+ 	|0| * 2 ^ 3 = |0| * 8	= 0
		+ 	|1| * 2 ^ 2 = |1| * 4	= 4
		+ 	|0| * 2 ^ 1 = |0| * 2 	= 0
		+ 	|0| * 2 ^ 0 = |0| * 1 	= 0
		_______________________________
			64 + 4 = 68
```

### Вычитание 121(10) - 68(10)

	
* 1. Обратный код для 68(10)
```
		NOT(01000100)
		  = 10111011		= (-1)*(32+16+8+2+1) = (-59) или 68-128=-59
```
* 2. Дополнительный код
```
		10111011
	+	00000001
		____________
		10111100			
```
* 3. Сложение 
```
		01111001(=121)
	+	10111100(=-60)
	_	______________
	    (1)00110101
```	
* 4. Проверка:
```
			|0| * 2 ^ 7 = |0| * 128 = 0
		+	|0| * 2 ^ 6 = |0| * 64	= 0
		+	|1| * 2 ^ 5 = |1| * 32	= 32
		+ 	|1| * 2 ^ 4 = |1| * 16 	= 16
		+ 	|0| * 2 ^ 3 = |0| * 8	= 0
		+ 	|1| * 2 ^ 2 = |1| * 4	= 4
		+ 	|0| * 2 ^ 1 = |0| * 2 	= 0
		+ 	|1| * 2 ^ 0 = |1| * 1 	= 1
		___________________________________
			32 + 16 + 4 + 1 = 53
```
	
