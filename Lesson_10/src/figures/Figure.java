package figures;

abstract class Figure {
    protected int x;
    protected int y;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }

    double getPerimeter() {
        return 0;
    }

    void paint() {
        System.out.println("Figure.");
        System.out.println("Coordinates: " + this.x + ", y: " + this.y);
    }
}
