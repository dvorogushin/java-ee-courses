package figures;

public interface MoveFigure {

    /**
     * Descriptive of moveFigureToZero
     *
     * Move the figure to coordinates 0, 0
     */
    void moveFigureToZero();

    /**
     * Descriptive of moveFigureTo
     *
     * Move the figure to coordinates x, y
     *
     * @param x (int) coordinate of x-axis
     * @param y (int) coordinate of y-axis
     */
    void moveFigureTo(int x, int y);
}

