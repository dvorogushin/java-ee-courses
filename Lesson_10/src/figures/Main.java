package figures;

public class Main {

    public static void main(String[] args) {

        Figure[] figures = new Figure[4];
//        figures[0] = new Figure(4,5);
        figures[1] = new Ellipse(4,5,10.0, 20.0);
        figures[2] = new Rectangle(40, 9,10.0, 15.0);
        figures[3] = new Circle(6,8,45);
        figures[0] = new Square(23,80,16);

        //Calling .paint() and .getPerimeter() methods via Figure object
        for (Figure fig:figures) {
            fig.paint();
            System.out.println("Perimeter: " + String.format("%,.1f",fig.getPerimeter()));
            System.out.println();
        }

        //calling .moveFigureToZero() method
        Circle circle = new Circle(6,7, 59);
        circle.paint();
        circle.moveFigureToZero();
        circle.paint();

        //calling .moveFigureTo() method
        Square square = new Square(5,8, 4);
        square.paint();
        square.moveFigureTo(2,2);
        square.paint();

        System.out.println(square.toString());
        System.out.println(circle.toString());
        System.out.println(figures[3].toString());
    }
}
