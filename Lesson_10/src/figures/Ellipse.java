package figures;

class Ellipse extends Figure {

    protected double radius1;
    private double radius2;

    //setter for radius1
    public void setRadius1(double radius1) {
        if (radius1 < 0) {
            radius1 *= -1;
        }
        this.radius1 = radius1;
    }

    //setter for radius2
    public void setRadius2(double radius2) {
        if (radius2 < 0) {
            radius2 *= -1;
        }
        this.radius2 = radius2;
    }

    // getter for radius1
    public double getRadius1() {
        return radius1;
    }

    // getter for radius2
    public double getRadius2() {
        return radius2;
    }

    //constructor full
    public Ellipse(int x, int y, double radius1, double radius2) {
        super(x, y);
        setRadius1(radius1);
        setRadius2(radius2);
    }

    //constructor with only radius1 & radius2, without coordinates
    public Ellipse(double radius1, double radius2) {
        super(0, 0);
        setRadius1(radius1);
        setRadius2(radius2);
    }

    //constructor with only coordinates, without radiuses
    public Ellipse(int x, int y) {
        super(x, y);
        this.radius1 = 0;
        this.radius2 = 0;
    }

    //paint method
    public void paint() {
        System.out.println("Figure: Ellipse.");
        System.out.println("Coordinates: " + this.x + ", " + this.y);
        System.out.println("Radius1: " + this.radius1 + ", radius2: " + this.getRadius2());
    }

    //getPerimeter method
    public double getPerimeter() {
        return Math.PI * (3 * (radius1 + radius2) - Math.sqrt((3 * radius1 + radius2) * (radius1 + 3 * radius2)));
    }

}
