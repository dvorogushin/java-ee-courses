package figures;

public class Circle extends Ellipse implements MoveFigure {

    //full constructor
    public Circle(int x, int y, double radius1) {
        super(x, y, radius1, radius1);
    }

    //constructor with radius only
    public Circle(double radius1) {
        super(radius1, radius1);
    }

    //constructor with coordinates only
    public Circle(int x, int y) {
        super(x, y);
    }

    //paint method
    public void paint() {
        System.out.println("Figure: Circle.");
        System.out.println("Coordinates: " + this.x + ", " + this.y);
        System.out.println("Radius: " + this.radius1);
    }

    //getPerimeter method
    public double getPerimeter() {
        return 2 * Math.PI * this.radius1;
    }

    @Override
    //override moveFigureTo() method of MoveFigure interface
    public void moveFigureTo(int x, int y) {
        this.x = x;
        this.y = y;
        System.out.println("Circle was moved to " + this.x + ", " + this.y + ".");
    }

    @Override
    //override moveFigureToZero() method of MoveFigure interface
    public void moveFigureToZero() {
        this.x = 0;
        this.y = 0;
        System.out.println("Circle was moved to 0, 0.");
    }
}
