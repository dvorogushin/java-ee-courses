package figures;

public class Square extends Rectangle implements MoveFigure {

    //constructor with only one side
    public Square(double side1) {
        super(side1, side1);
    }

    //constructor with only coordinates
    public Square(int x, int y) {
        super(x, y);
    }

    //constructor with coordinates and one side
    public Square(int x, int y, double side1) {
        super(x, y, side1, side1);
    }

    @Override
    //override paint() method of Rectangle class
    public void paint() {
        System.out.println("Figure: Square.");
        System.out.println("Coordinates: " + this.x + ", " + this.y);
        System.out.println("Side: " + this.getSide1());
    }

    @Override
    //override moveFigureToZero() method of MoveFigure interface
    public void moveFigureToZero() {
        this.x = 0;
        this.y = 0;
        System.out.println("Square was moves to 0, 0.");
    }

    @Override
    //override moveFigureTo() method of MoveFigure interface
    public void moveFigureTo(int x, int y) {
        this.x = x;
        this.y = y;
        System.out.println("Square was moved to " + this.x + ", " + this.y + ".");
    }

}
