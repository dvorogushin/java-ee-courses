## Реализация абстрактного класса и интерфейса

* В проекте реализован абстрактный класс [Figure()](src/figures/Figure.java), от которого наследуются классы [Ellipse()](src/figures/Ellipse.java) и [Rectangle()](src/figures/Rectangle.java), от которых, в свою очередь, наследуются классы [Circle()](src/figures/Circle.java) и [Square()](src/figures/Square.java).

* В классах наследниках перегружены (override) методы класса родителя: .paint() и .getPerimeter().

* В проекте применен интерфейс [MoveFigure()](src/figures/MoveFigure.java), в котором указано обязательство реализующих классов (в данном проекте классов [Circle()](src/figures/Circle.java) и [Square()](src/figures/Square.java)) реализовать метод перемещения фигур в начало координат (.moveFigureToZero()) и в заданные координаты (.moveFigureTo()).
