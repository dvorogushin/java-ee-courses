package demo;

import java.lang.reflect.Field;

/**
 * Date: 05.01.2022
 * Project: ReflectionDemo
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class Main {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {

        // объектная переменная = экземпляр класса
        SomeClass someObject = new SomeClass();

        // получаем класс объекта
        Class<SomeClass> someClassAsClass = (Class < SomeClass>) someObject.getClass() ;

        // получаем private поле класса
        Field someField = someObject.getClass().getDeclaredField("somePrivateStringField");

        // получаем все поля класса
        Field[] fields = someClassAsClass.getDeclaredFields();

        // читаем тип и наименование полей
        for (Field field:fields) {
            System.out.println(field.getType() + " " + field.getName());
        }

        // установка значения private поля
        someField.setAccessible(true);
        someField.set(someObject, "Private, but readable");
        System.out.println(someField.get(someObject));
        System.out.println(someObject.getSomePrivateStringField());

     }


}
