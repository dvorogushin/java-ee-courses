package demo;

/**
 * Date: 05.01.2022
 * Project: ReflectionDemo
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class SomeClass {
    public int someField =3;

    public String getSomePrivateStringField() {
        return somePrivateStringField;
    }

    public String someStringField = "public";
    private String somePrivateStringField = "private";
}
