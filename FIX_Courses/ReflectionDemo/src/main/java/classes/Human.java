package classes;

/**
 * Date: 05.01.2022
 * Project: ReflectionDemo
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class Human {
    public int age;
    public String name;

    public Human(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public Human(String name) {
        this.name = name;
    }

    public Human(int age) {
        this.age = age;
    }

    public Human() {
        this.age = 3;
        this.name = "NAME";
    }
}
