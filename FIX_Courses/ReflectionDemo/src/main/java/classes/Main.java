package classes;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Date: 05.01.2022
 * Project: ReflectionDemo
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class Main {
    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException {

        Scanner scanner = new Scanner(System.in);
        String className = scanner.nextLine(); // classes.Human

        // по имени класса получаем объект типа Class
        Class aClass = Class.forName(className);
        Field[] fields = aClass.getDeclaredFields();
        Arrays.stream(fields).forEach(f -> System.out.println(f.getType()));

        // создание экземпляра класса (instance)
        System.out.println(aClass); // class classes.Human
        Object object = aClass.newInstance();
        System.out.println(object); //classes.Human@65ab7765

        // получение конструкторов и их параметров (сигнатуры),
        // кроме пустого конструктора (по умолчанию)
        Constructor[] constructors = aClass.getDeclaredConstructors();
        Arrays.stream(constructors)
                .forEach(constructor -> Arrays.stream(constructor
                        .getParameterTypes())
                        .forEach(pt -> System.out.println(pt.getName())));

    }


}
