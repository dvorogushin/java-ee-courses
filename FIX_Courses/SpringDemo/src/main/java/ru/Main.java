package ru;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Date: 05.01.2022
 * Project: SpringDemo
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("ru/context.xml");

//        MessageRenderer renderer = (MessageRenderer) context.getBean("renderer");

        IndependentMessageRenderer renderer = context.getBean(IndependentMessageRenderer.class);

        renderer.print();

    }
}
