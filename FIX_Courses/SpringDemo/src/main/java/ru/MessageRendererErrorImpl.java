package ru;

/**
 * Date: 05.01.2022
 * Project: SpringDemo
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class MessageRendererErrorImpl implements MessageRenderer{

    public Message message;

    public MessageRendererErrorImpl(Message message) {
        this.message = message;
    }

    @Override
    public void printMessage() {
        System.err.println(message.getText());
    }
}
