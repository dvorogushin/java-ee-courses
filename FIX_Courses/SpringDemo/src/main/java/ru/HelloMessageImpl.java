package ru;

/**
 * Date: 05.01.2022
 * Project: SpringDemo
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class HelloMessageImpl implements Message {

    private String text;

    public HelloMessageImpl(String text) {
        this.text = "Hello " + text;
    }

    @Override
    public String getText() {
        return text;
    }
}
