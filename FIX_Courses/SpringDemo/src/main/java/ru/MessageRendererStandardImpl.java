package ru;

/**
 * Date: 05.01.2022
 * Project: SpringDemo
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class MessageRendererStandardImpl  implements MessageRenderer{

    public Message message;

    public MessageRendererStandardImpl(Message message) {
        this.message = message;
    }

    @Override
    public void printMessage() {
        System.out.println(message.getText());
    }
}
