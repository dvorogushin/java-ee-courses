package ru;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * Date: 05.01.2022
 * Project: SpringDemo
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class IndependentMessageRenderer {

 // автоматически сканирует context.xml на наличие соответствующего бина
 @Autowired
 private MessageRenderer renderer;

// public IndependentMessageRenderer(MessageRenderer renderer) {
//  this.renderer = renderer;
// }

 public void print() {
  renderer.printMessage();
 }
}
