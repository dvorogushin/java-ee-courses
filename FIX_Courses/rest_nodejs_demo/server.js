// отправляет заполненую форму login на сервер (java)
// с полученным токеном получает список юзеров
// токен - ключ аутентификации

// подключаем библиотеки
let express = require('express');
let cookieParser = require('cookie-parser');
let bootstrap = require('bootstrap-4');

// создаем экземпляр express-server
let app = express();

// используем cookieParser
app.use(cookieParser());

// если пользователь запросил страницу users.html
app.get('/users.html', function (req, res, next) {

    // смотрим, если в куках token
    let token = req.cookies['Auth-Token'];

    // если token'а нет - перенаправляем на login
    if (!req.cookies['Auth-Token']) {
        res.redirect('/login.html')
    }
    next();
});

// раздаем папку public
app.use(express.static('public'));
app.listen(8082);
console.log("StaticServer started...");