// делает запрос на сервер на получение пользователей
// в запрос вставляет полученный только что токен
// для подтверждения аутентификации

function getUsers() {
    $.ajax({
        url: 'http://localhost:8081' + '/users?token=' + getCookie("Auth-Token"),
        type: 'get',
        success: function (data, textStatus, request) {
            const table = document.getElementById("users_table");
            for (let i = 0; i < data.length; i++) {
                let row = table.insertRow(i+1);
                const cellFirstName = row.insertCell(0);
                const cellLastName = row.insertCell(1);
                const cellRole = row.insertCell(2);
                const cellState = row.insertCell(3);
                cellFirstName.innerHTML = data[i]["firstName"];
                cellLastName.innerHTML = data[i]["lastName"];
                cellRole.innerHTML = data[i]["role"];
                cellState.innerHTML = data[i]["state"];
            }
        }
    })
}

function getCookie(name) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
}
