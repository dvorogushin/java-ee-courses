package ru.example;

/**
 * Date: 05.01.2022
 * Project: SpringDemo
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class ByeMessageImpl implements Message {

    private String text;

    public ByeMessageImpl(String text) {
        this.text = "Bye " + text;
    }

    @Override
    public String getText() {
        return text;
    }
}
