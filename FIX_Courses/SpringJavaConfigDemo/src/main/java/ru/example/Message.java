package ru.example;

/**
 * Date: 05.01.2022
 * Project: SpringDemo
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */

public interface Message {
    String getText();
}
