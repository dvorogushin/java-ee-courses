package ru.example;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Date: 05.01.2022
 * Project: SpringJavaConfigDemo
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@ComponentScan(basePackages = "ru.example")
@Configuration
public class AppConfig {

    @Bean
    Message message() {
        return new HelloMessageImpl("Andrey");
    }

    @Bean
    public MessageRenderer messageRenderer() {
        return new MessageRendererErrorImpl(message());
    }
}
