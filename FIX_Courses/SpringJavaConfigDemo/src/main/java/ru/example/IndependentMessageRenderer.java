package ru.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Date: 05.01.2022
 * Project: SpringDemo
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Component
public class IndependentMessageRenderer {

 // автоматически сканирует context.xml на наличие соответствующего бина
 @Autowired
 private MessageRenderer renderer;

// конструктор нужен если нет @Autowired
// public ru.IndependentMessageRenderer(ru.MessageRenderer renderer) {
//  this.renderer = renderer;
// }

 public void print() {
  renderer.printMessage();
 }
}
