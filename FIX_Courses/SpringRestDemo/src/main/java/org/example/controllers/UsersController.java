package org.example.controllers;

import lombok.RequiredArgsConstructor;
import org.example.forms.UserForm;
import org.example.transfer.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.example.models.User;
import org.example.services.UserService;

import java.util.List;

import static org.example.transfer.UserDto.from;

/**
 * Date: 18.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@RestController
@RequiredArgsConstructor
public class UsersController {

    @Autowired
    private final UserService userService;

    // отправка всех юзеров
    @GetMapping("/users")
    public List<UserDto> getUsers() {
        List<User> users = userService.getUsers();
        return from(users);
    }

    // отправка юзера
    @GetMapping("/users/{user-id}")
    public UserDto getUser(@PathVariable("user-id") Long userId) {
        User user = userService.getUser(userId);
        return UserDto.from(user);
    }

    // добавление юзера, получение JSON объекта
    @PostMapping("/users")
    public ResponseEntity<Object> addUser(@RequestBody UserForm form) {
        userService.signUp(form);
        return ResponseEntity.ok().build();
    }
}
