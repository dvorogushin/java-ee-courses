package org.example.controllers;

import org.example.forms.LoginForm;
import org.example.services.LoginService;
import org.example.transfer.TokenDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Date: 07.01.2022
 * Project: SpringRestDemo
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;

    @PostMapping("/login")
    public ResponseEntity<TokenDto> login(@RequestBody LoginForm form) {
        return ResponseEntity.ok(loginService.login(form));
    }
}

