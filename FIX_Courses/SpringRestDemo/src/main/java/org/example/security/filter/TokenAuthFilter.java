package org.example.security.filter;

import org.example.security.token.TokenAuthentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Date: 07.01.2022
 * Project: SpringRestDemo
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Component
public class TokenAuthFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Filter.super.init(filterConfig);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        // явное преобразование request
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String token = request.getParameter("token");

        // создали объект TokenAuthentication для передачи в Spring
        TokenAuthentication tokenAuthentication = new TokenAuthentication(token);

        if (token == null) {
            // аутентификация не пройдена
            tokenAuthentication.setAuthenticated(false);
        } else {
            // передача в Spring аутентификации =
            // указания Spring'у, что у клиента есть аутентификация (token)
            SecurityContextHolder.getContext().setAuthentication(tokenAuthentication);
        }

        // передача запроса дальше
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
        Filter.super.destroy();
    }
}
