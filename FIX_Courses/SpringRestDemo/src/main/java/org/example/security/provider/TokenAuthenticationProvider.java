package org.example.security.provider;

import org.example.models.Token;
import org.example.repositories.TokenRepository;
import org.example.security.details.UserDetailsServiceImpl;
import org.example.security.token.TokenAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import java.util.Optional;

/**
 * Date: 07.01.2022
 * Project: SpringRestDemo
 * <p>
 * Непосредственно аутентификация
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Component
public class TokenAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private TokenRepository tokenRepository;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    // тип аутентификации (в нашем случае token, а не login-password)
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        TokenAuthentication tokenAuthentication = (TokenAuthentication) authentication;

        // поиск token'а в базе
        Optional<Token> tokenCandidate =
                tokenRepository.findOneByValue(tokenAuthentication.getName());

        // если токен присутствует, вытаскиваем UserDetails и подтверждаем аутентификацию
        if (tokenCandidate.isPresent()) {
            UserDetails userDetails =
                    userDetailsService.loadUserByUsername(tokenCandidate.get().getUser().getLogin());
            tokenAuthentication.setUserDetails(userDetails);
            tokenAuthentication.setAuthenticated(true);

            // отдаем аутентификацию в Security
            return tokenAuthentication;
        } else {
            throw new IllegalArgumentException("Bad token");
        }
    }

    // какие классы поддерживает провайдер аутентификации
    @Override
    public boolean supports(Class<?> authentication) {

        return TokenAuthentication.class.equals(authentication);
    }
}
