package org.example.forms;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Date: 26.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Getter
@Setter
@Builder
public class LoginForm {

    private String login;

    private String password;
}
