package org.example.services.impl;

import org.apache.commons.lang3.RandomStringUtils;
import org.example.forms.LoginForm;
import org.example.models.Token;
import org.example.models.User;
import org.example.repositories.TokenRepository;
import org.example.repositories.UserRepository;
import org.example.services.LoginService;
import org.example.transfer.TokenDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Random;

import static org.example.transfer.TokenDto.from;

/**
 * Date: 07.01.2022
 * Project: SpringRestDemo
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private TokenRepository tokenRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Override
    public TokenDto login(LoginForm loginForm) {

        Optional<User> userCandidate = userRepository.findOneByLogin(loginForm.getLogin());

        if (userCandidate.isPresent()) {

            // если в форме присутствует логин/пароль
            User user = userCandidate.get();

            // сравниваем пароли
            if (passwordEncoder.matches(loginForm.getPassword(), user.getHashPassword())) {

                // создаем токен
                Token token = Token.builder()
                        .user(user)
                        .value(RandomStringUtils.random(10, true, true))
                        .build();

                tokenRepository.save(token);
                return from(token);
            }
        }  throw new IllegalArgumentException("User not found");
    }
}
