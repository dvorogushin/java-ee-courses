package org.example.services;

import org.example.forms.LoginForm;
import org.example.models.Token;
import org.example.transfer.TokenDto;

/**
 * Date: 07.01.2022
 * Project: SpringRestDemo
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */

public interface LoginService {
    TokenDto login(LoginForm loginForm);
}
