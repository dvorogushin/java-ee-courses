package org.example.services.impl;

import lombok.RequiredArgsConstructor;
import org.example.forms.UserForm;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.example.models.User;
import org.example.repositories.UserRepository;
import org.example.services.UserService;

import java.util.List;

/**
 * Date: 18.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;

    @Override
    public User getUser(Long id) {
        User user = userRepository.getById(id);
        return user;
    }

    @Override
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    @Override
    public void signUp(UserForm form) {
        User user = User.builder()
                .firstName(form.getFirstName())
                .lastName((form.getLastName()))
                .login(form.getLogin())
                .hashPassword(passwordEncoder.encode(form.getPassword()))
                .role(User.Role.USER)
                .state(User.State.NOT_CONFIRMED)
                .build();
        userRepository.save(user);
    }

}
