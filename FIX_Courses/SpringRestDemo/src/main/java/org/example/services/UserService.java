package org.example.services;

import org.example.forms.UserForm;
import org.example.models.User;

import java.util.List;

/**
 * Date: 18.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */


public interface UserService {
    User getUser(Long userId);


    List<User> getUsers();

    void signUp(UserForm form);
}
