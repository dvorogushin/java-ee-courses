package org.example.models;

import lombok.*;

import javax.persistence.*;
import java.util.List;

/**
 * Date: 15.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity(name = "account")
//@ToString(exclude = {"createdCompanies", "createdProducts", "createdOrders", "paidOrders"})
public class User {

    public enum Role {
        USER, ADMIN;
    }

    public enum State {
        NOT_CONFIRMED, CONFIRMED, BANNED, DELETED;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition = "VARCHAR (30) NOT NULL UNIQUE")
    private String login;

    @Column(columnDefinition = "VARCHAR (100) NOT NULL")
    private String hashPassword;

    @Column(columnDefinition = "VARCHAR (20)")
    private String firstName;

    @Column(columnDefinition = "VARCHAR (20)")
    private String lastName;

    @Enumerated(value = EnumType.STRING)
    @Column(columnDefinition = "VARCHAR (20) NOT NULL")
    private State state;

    @Enumerated(value = EnumType.STRING)
    @Column(columnDefinition = "VARCHAR (20) NOT NULL")
    private Role role;

    @OneToMany(mappedBy = "user")
    @ToString.Exclude
    private List<Token> tokens;


}
