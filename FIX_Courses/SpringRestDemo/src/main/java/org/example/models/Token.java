package org.example.models;

import lombok.*;

import javax.persistence.*;

/**
 * Date: 15.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity(name = "token")
//@ToString(exclude = {"createdCompanies", "createdProducts", "createdOrders", "paidOrders"})
public class Token {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String value;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
