package org.example.transfer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.example.models.Token;
import org.example.models.User;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Date: 07.01.2022
 * Project: SpringRestDemo
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Data
@AllArgsConstructor
@Builder
public class UserDto {

    private String firstName;
    private String lastName;
    private String role;
    private String state;

    public static UserDto from(User user) {
        return UserDto.builder()
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .role(user.getRole().toString())
                .state(user.getState().toString())
                .build();
    }

    public static List<UserDto> from(List<User> users) {
        return users.stream().map(UserDto::from).collect(Collectors.toList());
    }

}
