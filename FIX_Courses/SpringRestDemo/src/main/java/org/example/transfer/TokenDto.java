package org.example.transfer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.example.models.Token;

/**
 * Date: 07.01.2022
 * Project: SpringRestDemo
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Data
@AllArgsConstructor
public class TokenDto {

    private String value;

    public static TokenDto from(Token token) {
        return new TokenDto(token.getValue());
    }

}
