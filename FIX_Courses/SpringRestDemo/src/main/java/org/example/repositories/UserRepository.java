package org.example.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.example.models.User;

import java.util.Optional;

/**
 * Date: 18.12.2021
 * Project: internet-shop
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findOneByLogin(String login);
}
