package example2;

/**
 * Date: 24.11.2021
 * Project: Lesson_21
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class Main {
    public static void main(String[] args) {

        Runnable runnable1 = () -> {
            for (int i = 1; i < 1_000_000_000; i++) {
                System.out.println("task A");
            }
        };
        Runnable runnable2 = () -> {
            for (int i = 1; i < 1_000_000_000; i++) {
                System.out.println("task B");
            }
        };
        Runnable runnable3 = () -> {
            for (int i = 1; i < 1_000_000_000; i++) {
                System.out.println("task C");
            }
        };

        Thread threadA = new Thread(runnable1);
        Thread threadB = new Thread(runnable2);
        Thread threadC = new Thread(runnable3);

        threadA.setName("A");
        threadB.setName("B");
        threadC.setName("C");

        // двойной старт почему-то не дает запуститься потоку, следующему за вторым стартом !?
        threadB.start();
        threadA.start();
        threadA.start( );
        threadC.start();

        for (int i = 1; i < 1_000_000_000; i++) {
            System.out.println("task Main");
        }


    }
}
