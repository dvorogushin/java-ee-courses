package homework;

import java.util.concurrent.TimeUnit;

import static homework.Main.array;
import static homework.Main.sum;

/**
 * Date: 24.11.2021
 * Project: Lesson_21
 * <p>
 * Класс представляет тред.
 * Внутри производится подсчет суммы чисел определенного диапазона глобального массива array[]
 * Сумма складывается в глобальный массив sum[] по индексу треда.
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class SumThread extends Thread {
    private int from;
    private int to;
    private int threadNumber;

    public SumThread(int from, int to, int threadNumber) {
        this.from = from;
        this.to = to;
        this.threadNumber = threadNumber;
    }

    @Override
    public void run() {
        for (int i = from; i <= to; i++) {
            sum[threadNumber] += array[i];
//            try {
//                TimeUnit.MICROSECONDS.sleep(1);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
        }
    }
}
