package homework;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Date: 24.11.2021
 * Project: Lesson_21
 * В main формируется массив случайных чисел, вычисляется сумма элементов массива.
 * Также запускается заданное количество тредов,
 * в которые передаются данные для подсчета суммы равного диапазона элементов глобального массива array[].
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class Main {

    // массив сумм для хранения результата подсчета в тредах
    public static int[] sum;
    // массив случайных чисел
    public static int[] array;

    public static void main(String[] args) {

        // запрашиваем размер массива
        System.out.print("\nEnter array size: ");
        Scanner scanner = new Scanner(System.in);
        int arraySize = scanner.nextInt();

        // запрашиваем количество тредов
        System.out.print("\nEnter quantity of threads: ");
        int threadsQuantity = scanner.nextInt();
        sum = new int[threadsQuantity];

        // формирование массива случайных чисел и подсчет суммы элементов
        Random random = new Random();
        array = new int[arraySize];
        int realSum = 0;
        for (int i = 0; i < arraySize; i++) {
            array[i] = random.nextInt(100);
            realSum += array[i];
        }

        // формирование коллекции тредов, распределение диапазонов глобального массива
        int partialArraySize = arraySize / threadsQuantity; // размер блока элементов для каждого треда
        int from = 0;
        int to = -1;    // на случай, если тред всего 1
        List<SumThread> threads = new ArrayList<>();        // коллекция тредов
        for (int i = 0; i < threadsQuantity - 1; i++) {     // цикл по всем тредам, кроме последнего
            from = partialArraySize * i;
            to = from + partialArraySize - 1;
            threads.add(new SumThread(from, to, i));
        }
        // последний тред - на случай если количество тредов
        // не кратно количеству элементов в глобальном массиве
        threads.add(new SumThread(to + 1, arraySize - 1, threadsQuantity - 1));

        // запускаем run() каждого треда
        for (Thread thread : threads) {
            thread.start();
        }

        // ожидаем выполнение операций в каждом треде
        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // суммирование результатов каждого треда
        int threadSum = 0;
        for (int i : sum) {
            threadSum += i;
        }
        // сравнение результатов подсчета сумм в главном потоке и в тредах
        boolean check = (realSum == threadSum);
        System.out.println(check);
        int i = 0;
    }
}
