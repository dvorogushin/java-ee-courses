package example1;

import java.util.Scanner;

public class NotMain {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        // создаем тред
        Thread thread = new OtherThread("example1.OtherThread.");
        // запускаем тред
        thread.start();

        for (int i = 0; i < 1_000_000_000; i++) {
            System.out.println(Thread.currentThread().getName());
        }
    }
}
