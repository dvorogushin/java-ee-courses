package example1;

/**
 * Date: 24.11.2021
 * Project: Lesson_21
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
public class OtherThread extends Thread {

    // присваиваем название треду
    public OtherThread(String name) {
        super(name);
    }

    // выполнение
    @Override
    public void run() {
        for(int i = 0; i < 1_000_000_000; i++)
        {
            System.out.println(Thread.currentThread().getName());
        }
    }
}
